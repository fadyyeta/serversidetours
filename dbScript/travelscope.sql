-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2020 at 04:35 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travelscope`
--

-- --------------------------------------------------------

--
-- Table structure for table `bannerslider`
--

CREATE TABLE `bannerslider` (
  `ID` int(255) NOT NULL,
  `title` varchar(1111) NOT NULL,
  `minPrice` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bannerslider`
--

INSERT INTO `bannerslider` (`ID`, `title`, `minPrice`) VALUES
(1, 'fly to London', 199),
(2, 'fly to USA', 499),
(3, 'fly to Canada', 299);

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `ID` int(255) NOT NULL,
  `URL` varchar(1111) NOT NULL,
  `datePosted` date NOT NULL,
  `title` varchar(1111) NOT NULL,
  `description` varchar(1111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`ID`, `URL`, `datePosted`, `title`, `description`) VALUES
(1, 'images/blog/img-01.jpg', '2020-04-01', '10 things todo before travel', 'Consectetur adipisicing elit, sed do eiusmod\r\n                              tempor incididunt ut labore et dolore magna aliqua\r\n                              enimad minim veniam, quis nostrud...'),
(2, 'images/blog/img-02.jpg', '2020-04-02', '10 things todo before travel', 'Consectetur adipisicing elit, sed do eiusmod\r\n                              tempor incididunt ut labore et dolore magna aliqua\r\n                              enimad minim veniam, quis nostrud...'),
(3, 'images/blog/img-01.jpg', '2020-04-03', '10 things todo before travel', 'Consectetur adipisicing elit, sed do eiusmod\r\n                              tempor incididunt ut labore et dolore magna aliqua\r\n                              enimad minim veniam, quis nostrud...'),
(4, 'images/blog/img-02.jpg', '2020-04-04', '10 things todo before travel', 'Consectetur adipisicing elit, sed do eiusmod\r\n                              tempor incididunt ut labore et dolore magna aliqua\r\n                              enimad minim veniam, quis nostrud...');

-- --------------------------------------------------------

--
-- Table structure for table `chooseus`
--

CREATE TABLE `chooseus` (
  `ID` int(255) NOT NULL,
  `URL` varchar(1111) NOT NULL,
  `URLVideo` varchar(1111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chooseus`
--

INSERT INTO `chooseus` (`ID`, `URL`, `URLVideo`) VALUES
(1, 'images/choose-us-1.jpg', 'https://www.youtube.com/watch?v=wJF5NXygL4k'),
(2, 'images/choose-us-2.jpg', 'https://www.youtube.com/watch?v=UGnrT0F-Igs&amp;nohtml5=False'),
(3, 'images/choose-us-3.jpg', 'https://www.youtube.com/watch?v=Scxs7L0vhZ4&amp;nohtml5=False');

-- --------------------------------------------------------

--
-- Table structure for table `cstfeedback`
--

CREATE TABLE `cstfeedback` (
  `ID` int(255) NOT NULL,
  `URL` varchar(1111) NOT NULL,
  `title` varchar(1111) NOT NULL,
  `cstComment` varchar(1111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cstfeedback`
--

INSERT INTO `cstfeedback` (`ID`, `URL`, `title`, `cstComment`) VALUES
(1, 'images/testimonials/thumb-01.jpg', 'Jennifer Doe & family', 'Adipisicing elit, sed do eiusmod tempor\r\n                                incididaut labore etolore magna aliqua. Ut enim\r\n                                ad minim am quis nostrud exercitation\r\n                                ullamco'),
(2, 'images/testimonials/thumb-02.jpg', 'Jennifer Doe & family', 'Adipisicing elit, sed do eiusmod tempor\r\n                                incididaut labore etolore magna aliqua. Ut enim\r\n                                ad minim am quis nostrud exercitation\r\n                                ullamco'),
(3, 'images/testimonials/thumb-03.jpg', 'Jennifer Doe & family', 'Adipisicing elit, sed do eiusmod tempor\r\n                                incididaut labore etolore magna aliqua. Ut enim\r\n                                ad minim am quis nostrud exercitation\r\n                                ullamco');

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE `destinations` (
  `ID` int(255) NOT NULL,
  `URL` varchar(1111) NOT NULL,
  `title` varchar(1111) NOT NULL,
  `flatIcon` varchar(1111) NOT NULL,
  `description` varchar(1111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `destinations`
--

INSERT INTO `destinations` (`ID`, `URL`, `title`, `flatIcon`, `description`) VALUES
(1, 'images/destination/img-01.jpg', 'rome', 'flaticon-colosseum', 'At vero eos et accusamus etsto odio dignissimos ducimus'),
(2, 'images/destination/img-02.jpg', 'new york', 'flaticon-eeuu2', 'At vero eos et accusamus etsto odio dignissimos ducimus'),
(3, 'images/destination/img-03.jpg', 'Germany', 'flaticon-france1', 'At vero eos et accusamus etsto odio dignissimos ducimus');

-- --------------------------------------------------------

--
-- Table structure for table `footergallery`
--

CREATE TABLE `footergallery` (
  `ID` int(255) NOT NULL,
  `URL` varchar(1111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `footergallery`
--

INSERT INTO `footergallery` (`ID`, `URL`) VALUES
(1, 'images/gallery/thumb/img-01.jpg'),
(2, 'images/gallery/thumb/img-02.jpg'),
(3, 'images/gallery/thumb/img-03.jpg'),
(4, 'images/gallery/thumb/img-04.jpg'),
(5, 'images/gallery/thumb/img-05.jpg'),
(6, 'images/gallery/thumb/img-06.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `loginuser`
--

CREATE TABLE `loginuser` (
  `ID` int(255) NOT NULL,
  `userName` varchar(50) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `eMail` varchar(50) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `imgURL` varchar(1111) NOT NULL,
  `twitterURL` varchar(255) NOT NULL,
  `linkedInURL` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `loginuser`
--

INSERT INTO `loginuser` (`ID`, `userName`, `pass`, `eMail`, `firstName`, `lastName`, `imgURL`, `twitterURL`, `linkedInURL`) VALUES
(1, 'fadyyeta', '123456', 'fadyyeta@gmail.com', 'Fady', 'Yeta', 'arya starkk.png', 'fadyyeta', 'https://www.linkedin.com/in/fadyyeta');

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `ID` int(255) NOT NULL,
  `URL` varchar(1111) NOT NULL,
  `title` varchar(1111) NOT NULL,
  `infoText` text NOT NULL,
  `discount` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`ID`, `URL`, `title`, `infoText`, `discount`) VALUES
(1, 'images/packages/img-01.jpg', 'Nights of london', 'Consectetur adipisicing elit sesmod tempor incididunt ut\r\n                        labore etlore magna aliquad minim', 15),
(2, 'images/packages/img-02.jpg', 'family fun in malaysia', 'Consectetur adipisicing elit sesmod tempor incididunt ut\r\n                        labore etlore magna aliquad minim', 23),
(3, 'images/packages/img-03.jpg', 'charm in dubai', 'Consectetur adipisicing elit sesmod tempor incididunt ut\r\n                      labore etlore magna aliquad minim.', 10),
(4, 'images/packages/img-04.jpg', 'journey of hope', 'Consectetur adipisicing elit sesmod tempor incididunt ut\r\n                      labore etlore magna aliquad minim.', 37);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bannerslider`
--
ALTER TABLE `bannerslider`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `chooseus`
--
ALTER TABLE `chooseus`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `cstfeedback`
--
ALTER TABLE `cstfeedback`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `footergallery`
--
ALTER TABLE `footergallery`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `loginuser`
--
ALTER TABLE `loginuser`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bannerslider`
--
ALTER TABLE `bannerslider`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `chooseus`
--
ALTER TABLE `chooseus`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cstfeedback`
--
ALTER TABLE `cstfeedback`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `destinations`
--
ALTER TABLE `destinations`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `footergallery`
--
ALTER TABLE `footergallery`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `loginuser`
--
ALTER TABLE `loginuser`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `ID` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
