<?php
//session_start();
require_once("php/includeHead.php");
require_once("php/includeScript.php");
require_once("php/db.php");
require_once("php/navHeader.php");
require_once("php/footer.php");
require_once("php/login.php");
$packagesFooter = ReadGeneral('packages', '*', '', 'LIMIT 3');
$galleryFooter = ReadGeneral('footergallery', '*', '', 'LIMIT 6');

?>
<!DOCTYPE html>

<head>
	<?php headInclude(' | About Us') ?>
	
</head>

<body>
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="wrapper" class="tg-haslayout">
		<?php navBar(); ?>
		<div class="tg-banner tg-haslayout parallax-window" data-parallax="scroll" data-bleed="100" data-speed="0.2" data-image-src="images/parallax-bg/parallax-img-02.jpg">
			<div class="tg-banner-content">
				<div class="container">
					<ol class="tg-breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li class="active">about</li>
					</ol>
				</div>
			</div>
		</div>
		<main id="main" class="tg-haslayout tg-aboutus">
			<!--************************************
					About US Start
			*************************************-->
			<section class="tg-main-section tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="tg-aboutus-box tg-haslayout">
								<div class="tg-page-head">
									<h1>about travelon</h1>
									<h2>short overview </h2>
								</div>
								<div class="tg-description">
									<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et doloare magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco labrsis nisiut aliquip exea commodo consequat. Duis aute irure dolor in reprehenderit ate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim. </p>
								</div>
								<a class="tg-btn" href="#">meet the team</a>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="tg-about-video tg-haslayout">
								<iframe src="https://www.youtube.com/embed/UnKJL_ifwkk?rel=0&amp;showinfo=0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					About Us End
			*************************************-->
			<!--************************************
					Counter Start
			*************************************-->
			<section class="tg-main-section tg-haslayout tg-bglight">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="tg-counters tg-haslayout">
								<div class="row">
									<div class="col-sm-6 col-xs-6 tg-marginbottom">
										<div class="tg-counter">
											<i class="tg-icon flaticon-favorite31"></i>
											<div class="tg-countercontent">
												<span class="tg-timer" data-from="0" data-to="3700" data-speed="8000" data-refresh-interval="50">3700</span>
												<div class="tg-heading-border">
													<h3>happy clients</h3>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6 col-xs-6 tg-marginbottom">
										<div class="tg-counter">
											<i class="tg-icon flaticon-cups17"></i>
											<div class="tg-countercontent">
												<span class="tg-timer" data-from="0" data-to="3700" data-speed="8000" data-refresh-interval="50">3700</span>
												<div class="tg-heading-border">
													<h3>earn awards</h3>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6 col-xs-6 tg-marginbottom">
										<div class="tg-counter">
											<i class="tg-icon flaticon-list18"></i>
											<div class="tg-countercontent">
												<span class="tg-timer" data-from="0" data-to="3700" data-speed="8000" data-refresh-interval="50">3700</span>
												<div class="tg-heading-border">
													<h3>packages</h3>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6 col-xs-6 tg-marginbottom">
										<div class="tg-counter">
											<i class="tg-icon flaticon-group12"></i>
											<div class="tg-countercontent">
												<span class="tg-timer" data-from="0" data-to="3700" data-speed="8000" data-refresh-interval="50">3700</span>
												<div class="tg-heading-border">
													<h3>partners</h3>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="tg-amazingfacts tg-haslayout">
								<div class="tg-page-head">
									<h1>some amazing facts</h1>
									<h2>customer love us</h2>
								</div>
								<ul>
									<li>Consectetur adipisicing elit sed do eiusmod tempor incididunt.</li>
									<li>Labore et dolore magna aliqua enim ad minim veniam.</li>
									<li>Quis nostrud exercitation ullamco laboris ut aliquip exea commodo.</li>
									<li>Consequat duis aute irure dolor in reprehenderit in voluptate.</li>
									<li>Velit esse cillum dolore eu fugiat nulla pariatur.</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
				Counter End
		*************************************-->
		</main>
		<?php footer($packagesFooter, $galleryFooter); ?>

	</div>
	<?php loginModal(); ?>
	<?php scriptInclude(); ?>
</body>

</html>