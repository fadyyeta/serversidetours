<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->

<!-- Mirrored from 786themes.net/html/travel-on/comming-soon.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Feb 2020 13:58:15 GMT -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BootStrap HTML5 CSS3 Theme</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flaticon.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/final-countdown.css">
	<link rel="stylesheet" href="css/swiper.css">
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/prettyPhoto.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/transitions.css">
	<link rel="stylesheet" href="css/color.css">
	<link rel="stylesheet" href="css/responsive.css">
	<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="tg-comming-soon">
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="wrapper" class="tg-haslayout">
		<!--************************************
				Main Start
		*************************************-->
		<main id="main" class="tg-main-section tg-haslayout">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="tg-commingsoon-content tg-haslayout">
							<div class="tg-heading-border">
								<h2>Hold tight!</h2>
								<h3>we are coming very soon</h3>
							</div>
								<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore agna aliqua. Ut enim ad minim veniam quis nostrud exercitation.</p>
							<div class="tg-countdown tg-haslayout">
								<div class="tg-clock">
									<div class="col-md-3 col-sm-6 col-xs-6">
										<div class="clock-item clock-days countdown-time-value">
											<div id="canvas-days" class="clock-canvas"></div>
											<div class="text">
												<p class="type-days type-time">DAYS</p>
												<p class="val">0</p>
											</div>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-6">
										<div class="clock-item clock-hours countdown-time-value">
											<div id="canvas-hours" class="clock-canvas"></div>
											<div class="text">
												<p class="type-hours type-time">HOURS</p>
												<p class="val">0</p>
											</div>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-6">
										<div class="clock-item clock-minutes countdown-time-value">
											<div id="canvas-minutes" class="clock-canvas"></div>
											<div class="text">
												<p class="type-minutes type-time">MINUTES</p>
												<p class="val">0</p>
											</div>
										</div>
									</div>
									<div class="col-md-3 col-sm-6 col-xs-6">
										<div class="clock-item clock-seconds countdown-time-value">
											<div id="canvas-seconds" class="clock-canvas"></div>
											<div class="text">
												<p class="type-seconds type-time">SECONDS</p>
												<p class="val">0</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tg-copyright">
						<p>&copy; 2015 | All Rights Reserved</p>
					</div>
				</div>
			</div>
		</main>
		<!--************************************
				Main End
		*************************************-->
	</div>
	<!--************************************
			Wrapper End
	*************************************-->
	<script src="js/vendor/jquery-library.js"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="js/jquery-ui.js"></script>
	<script src="js/kinetic.js"></script>
	<script src="js/jquery.final-countdown.js"></script>
	<script src="js/swiper.js"></script>
	<script src="js/owl.carousel.js"></script>
	<script src="js/prettyPhoto.js"></script>
	<script src="js/parallax.js"></script>
	<script src="js/appear.js"></script>
	<script src="js/countTo.js"></script>
	<script src="js/gmap3.min.js"></script>
<script src="js/isotope.pkgd.js"></script>
<script src="js/walkway.js"></script>
	<script src="js/main.js"></script>
	<script>
		$('.tg-countdown').final_countdown({
			'start': 1362139200,
			'end': 1388461320,
			'now': 1387461319
		});
	</script>
</body>

<!-- Mirrored from 786themes.net/html/travel-on/comming-soon.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Feb 2020 13:58:17 GMT -->
</html>