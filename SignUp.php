<?php
//session_start();
require_once("php/includeHead.php");
require_once("php/includeScript.php");
require_once("php/db.php");
require_once("php/navHeader.php");
require_once("php/footer.php");
$packagesFooter = ReadGeneral('packages', '*', '', 'LIMIT 3');
$galleryFooter = ReadGeneral('footergallery', '*', '', 'LIMIT 6');

?>
<!DOCTYPE html>

<head>
    <?php headInclude(' | Register') ?>
    <link rel="stylesheet" href="css/signup.css" />
</head>

<body>

    <div id="wrapper" class="tg-haslayout">
        <?php navBar(); ?>
        <div class="tg-banner tg-haslayout parallax-window" data-parallax="scroll" data-bleed="100" data-speed="0.2" data-image-src="images/parallax-bg/parallax-img-02.jpg">

        </div>
        <main id="main" class="tg-haslayout">


            <div class="container signupForm" style="padding: 10vh 0;">
                <div class="row py-5 mt-4 align-items-center" style="margin: auto;">
                    <h2 class="text-center" style="font-size: 72pt; color: #4F6BB4;">Join us now</h2>
                    <!-- For Demo Purpose -->
                    <div class="col-md-5 pr-lg-5 mb-5 mb-md-0">
                        <img src="images/traveler-signup-form.png" alt="" class="img-fluid mb-3 d-none d-md-block">
                    </div>

                    <!-- Registeration Form -->
                    <div class="col-md-7 col-lg-6 ml-auto">
                        <form action="php/signUp.php" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="row" style="margin: 15px;">

                                    <!-- User Name -->
                                    <div class="input-group col-lg-12">
                                        <!-- <div class="input-group-prepend">
                                            <span class="input-group-text bg-white px-4 border-md border-right-0">
                                                <i class="fa fa-user text-muted"></i>
                                            </span>
                                        </div> -->
                                        <i class="fa fa-user text-muted"></i>
                                        <input id="userName" type="text" name="username" placeholder="User Name" class="form-control bg-white border-left-0 border-md">
                                    </div>

                                    <!-- Email Address -->
                                    <div class="input-group col-lg-12">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-white px-4 border-md border-right-0">
                                                <i class="fa fa-envelope text-muted"></i>
                                            </span>
                                        </div>
                                        <input id="email" type="email" name="email" placeholder="Email Address" class="form-control bg-white border-left-0 border-md">
                                    </div>

                                    <!-- Password -->
                                    <div class="input-group col-lg-12">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-white px-4 border-md border-right-0">
                                                <i class="fa fa-lock text-muted"></i>
                                            </span>
                                        </div>
                                        <input id="password" type="password" name="password" placeholder="Password" class="form-control bg-white border-left-0 border-md">
                                    </div>

                                    <!-- Password Confirmation -->
                                    <div class="input-group col-lg-12">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text bg-white px-4 border-md border-right-0">
                                                <i class="fa fa-lock text-muted"></i>
                                            </span>
                                        </div>
                                        <input id="passwordConfirmation" type="password" name="passwordConfirmation" placeholder="Confirm Password" class="form-control bg-white border-left-0 border-md">
                                    </div>
                                </div>
                                <!-- Submit Button -->
                                <div class="form-group col-lg-12 mx-auto mb-0">
                                    <button class="btn btn-primary btn-block py-2" type="submit" name="createProfile">Create your account</button>
                                </div>

                                <!-- Divider Text -->
                                <div class="form-group col-lg-12 mx-auto d-flex align-items-center my-4 text-center">
                                    <div class="border-bottom w-100 ml-5"></div>
                                    <span class="px-2 small text-muted font-weight-bold text-muted">OR</span>
                                    <div class="border-bottom w-100 mr-5"></div>
                                </div>

                                <!-- Social Login -->
                                <div class="form-group col-lg-12 mx-auto">
                                    <a href="#" class="btn btn-primary btn-block py-2 btn-facebook">
                                        <i class="fa fa-facebook-f mr-2"></i>
                                        <span class="font-weight-bold">Continue with Facebook</span>
                                    </a>
                                    <a href="#" class="btn btn-primary btn-block py-2 btn-twitter">
                                        <i class="fa fa-twitter mr-2"></i>
                                        <span class="font-weight-bold">Continue with Twitter</span>
                                    </a>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </main>
        <?php footer($packagesFooter, $galleryFooter); ?>
    </div>
    <?php scriptInclude(); ?>
    <script>
        // For Demo Purpose [Changing input group text on focus]
        $(function() {
            $('input, select').on('focus', function() {
                $(this).parent().find('.input-group-text').css('border-color', '#80bdff');
            });
            $('input, select').on('blur', function() {
                $(this).parent().find('.input-group-text').css('border-color', '#ced4da');
            });
        });
    </script>
    <script>
        $("#createProfile").click(function() {
            ProfCreate();
        });

        function ProfCreate() {
            //e.preventDefault();
            $.ajax({
                url: 'php/signUp.php',
                type: 'POST',
                data: $('form').serialize(),
                success: function(result) {
                    if (!result) {
                        return false;
                    }
                }
            });
        }
    </script>

</body>

</html>