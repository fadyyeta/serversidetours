<?php
function loopThrough($repeatLimit, array $items, $packName)
{
  $snippetResult = "";
  for ($x = 0; $x <= $repeatLimit; $x++) {
    //loop through all DB rows and create package card for each
    foreach ($items as $itemRow) {
      $snippetResult = $snippetResult . snippetForEach($itemRow, $packName);
    }
  }
  return $snippetResult;
}
function snippetForEach(array $item, $packName)
{
  $renderPackages = "";
  switch ($packName) {
    case "Packages":
      $renderPackages =
        //loop once to repeat 4 records in DB
        "<li>
          <h4>
            <a href='#'>" . $item["discount"] . "% off <span>" . $item["title"] . "</span></a>
          </h4>
          <p>" . $item["infoText"] . "</p>
        </li>";
      break;
    case "Gallery":
      $renderPackages = "
        <li>
          <figure>
            <img
              src='" . $item["URL"] . "'
              alt='image description'
            />
            <div class='tg-hover-pattran'>
              <a
                href='" . $item["URL"] . "'
                data-rel='prettyPhoto[gallery]'
                ><i class='flaticon-plus79'></i
              ></a>
            </div>
          </figure>
        </li>
      ";
      break;
  }
  return $renderPackages;
}
function footer(array $packages, array $gallery)
{
  $footer = "
      <footer id='footer' class='tg-footer tg-haslayout'>
        <div class='tg-getaquickestimate tg-haslayout tg-positionrelative'>
          <div class='container'>
            <div class='row'>
              <div class='col-xs-12'>
                <div class='tg-heading-border'>
                  <h2>Get a quick estimate!</h2>
                  <h3>Get a Proffesional help</h3>
                </div>
              </div>
              <form>
                <fieldset>
                  <div>
                    <span class='select'>
                      <select>
                        <option>Travel Purpose</option>
                        <option>Travel Purpose</option>
                        <option>Travel Purpose</option>
                      </select>
                    </span>
                  </div>
                  <div>
                    <input
                      type='text'
                      class='form-control'
                      placeholder='Travel Date'
                    />
                  </div>
                  <div>
                    <span class='select'>
                      <select>
                        <option>Number Of People</option>
                        <option>Number Of People</option>
                        <option>Number Of People</option>
                      </select>
                    </span>
                  </div>
                  <div>
                    <input
                      type='email'
                      class='form-control'
                      placeholder='Your Email'
                    />
                  </div>
                  <div>
                    <button type='submit' class='tg-btn tg-btn-lg'>
                      Calculate Now
                    </button>
                  </div>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
        <div class='tg-footerarea'>
          <div class='container'>
            <div class='row'>
              <div class='tg-columns tg-haslayout'>
                <div class='col-md-3 col-sm-6 col-xs-6'>
                  <div class='tg-column'>
                    <strong class='tg-logo'>
                      <a href='index.html'>
                        <img src='images/logo.png' alt='image description' />
                      </a>
                    </strong>
                    <div class='tg-description'>
                      <p>
                        Consectetur adipisicing elit sed do iuod tempor incididunt ut
                        labore...
                      </p>
                    </div>
                    <ul class='tg-socialicon'>
                      <li>
                        <a href='#'>
                          <i class='fa fa-facebook'></i>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i class='fa fa-twitter'></i>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i class='fa fa-pinterest-p'></i>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i class='fa fa-linkedin'></i>
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <i class='fa fa-tumblr'></i>
                        </a>
                      </li>
                    </ul>
                    <a class='tg-btn-more' href='#'>More</a>
                  </div>
                </div>
                <div class='col-md-4 col-sm-6 col-xs-6'>
                  <div class='tg-column'>
                    <div class='tg-heading-border'>
                      <h3>Fair Price Packages</h3>
                    </div>
                    <ul class='tg-pkgs'>
                      " . loopThrough(0, $packages, "Packages") . "
                    </ul>
                  </div>
                </div>
                <div class='col-md-2 col-sm-6 col-xs-6'>
                  <div class='tg-column'>
                    <div class='tg-heading-border'>
                      <h3>search for</h3>
                    </div>
                    <ul class='tg-links'>
                      <li><a href='#'>FLight</a></li>
                      <li><a href='#'>Train</a></li>
                      <li><a href='#'>Bus</a></li>
                      <li><a href='#'>Car</a></li>
                      <li><a href='#'>Cruise</a></li>
                      <li><a href='#'>Room</a></li>
                      <li><a href='#'>Holiday</a></li>
                    </ul>
                  </div>
                </div>
                <div class='col-md-3 col-sm-6 col-xs-6'>
                  <div class='tg-column'>
                    <div class='tg-heading-border'>
                      <h3>Gallery</h3>
                    </div>
                    <ul class='tg-gallery'>
                      " . loopThrough(0, $gallery, "Gallery") . "
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class='tg-copyright'>
          <div class='container'>
            <p>&copy; " .  date("Y") . " <span class='brandName'>Travel Scope</span> | All Rights Reserved</p>
          </div>
        </div>
      </footer>
    ";
  echo $footer;
}
