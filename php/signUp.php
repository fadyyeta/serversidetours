<?php
// Include the database configuration file
require_once("db.php");
session_start();

function createProfile()
{
    $conn = ConnectDB();

    $statusMsg = '';

    if (isset($_POST["createProfile"])) {
        //fields to be updated
        $userName = $_POST['username'];
        $eMail = $_POST['email'];
        $password = $_POST['password'];

        $insert = $conn->query("INSERT INTO loginuser (userName,eMail,pass) VALUES ('" . $userName . "','" . $eMail . "','" . $password . "')");
        if ($insert) {
            $statusMsg = "The profile has been created successfully.";
            $latestID = $conn->insert_id;
            $profile = ReadGeneral('loginuser', '*', '', 'where ID = ' . $latestID);
            $_SESSION['userName'] = $profile[0]['userName'];
            $_SESSION['pass'] = $profile[0]['pass'];
            $_SESSION['eMail'] = $profile[0]['eMail'];
            $_SESSION['userID'] = $profile[0]['ID'];
        } else {
            $statusMsg = "Profile creation failed, please try again.";
        }
    }
    mysqli_close($conn);
    header("Location: ../profile.php");
    exit();
}

createProfile();
