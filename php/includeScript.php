<?php
function scriptInclude()
{
    $scriptInclude = "
        <script src='js/vendor/jquery-library.js'></script>
        <script src='js/vendor/bootstrap.min.js'></script>
        <script src='http://maps.google.com/maps/api/js?sensor=false'></script>
        <script src='js/jquery-ui.js'></script>
        <script src='js/swiper.js'></script>
        <script src='js/owl.carousel.js'></script>
        <script src='js/prettyPhoto.js'></script>
        <script src='js/parallax.js'></script>
        <script src='js/appear.js'></script>
        <script src='js/countTo.js'></script>
        <script src='js/gmap3.min.js'></script>
        <script src='js/isotope.pkgd.js'></script>
        <script src='js/isotope.pkgd.js'></script>
        <script src='js/walkway.js'></script>
        <script src='js/main.js'></script>
    ";
    echo $scriptInclude;
}
