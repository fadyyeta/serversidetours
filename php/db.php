<?php
function ConnectDB()
{
    $serverName = "127.0.0.1";
    $userName = "travelscope";
    $password = "P@ssW)rd";
    $dbName = "travelscope";

    // create connection
    $con = mysqli_connect($serverName, $userName, $password, $dbName);

    //check connection
    if (!$con) {
        # code...
        die("connection failed: " . mysqli_connect_error());
    }
    // echo "connected";
    return $con;
}
function ReadGeneral($tableName, $cols, $orderBy, $condition)
{
    $conn = ConnectDB();
    $sql = "select " . $cols . " from " . $tableName . " " . $orderBy . " " . $condition;
    $result = mysqli_query($conn, $sql);
    $output = mysqli_fetch_all($result, MYSQLI_ASSOC);

    mysqli_free_result($result);
    mysqli_close($conn);

    return $output;
}
