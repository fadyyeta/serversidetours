<?php
// Include the database configuration file
require_once("db.php");
session_start();

function updateProfile()
{
    $conn = ConnectDB();

    $statusMsg = '';

    if (!empty($_FILES["file"]["name"])) {
        // File upload path
        $targetDir = "../uploads/";
        $fileName = basename($_FILES["file"]["name"]);
        $targetFilePath = $targetDir . $fileName;
        $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);

        // Allow certain file formats
        $allowTypes = array('jpg', 'png', 'jpeg', 'gif', 'pdf');
        if (in_array($fileType, $allowTypes)) {
            // Upload file to server
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)) {
                // Insert image file name into database
                $insert = $conn->query("UPDATE loginuser SET imgURL='" . $fileName . "' where ID = " . $_SESSION["userID"]);
                if ($insert) {
                    $statusMsg = "The file " . $fileName . " has been uploaded successfully.";
                } else {
                    $statusMsg = "File upload failed, please try again.";
                }
            } else {
                $statusMsg = "Sorry, there was an error uploading your file.";
            }
        } else {
            $statusMsg = 'Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload.';
        }
    }

    if (isset($_POST["updateProf"])) {
        //fields to be updated
        $userName = $_POST['userName'];
        $fName = $_POST['fName'];
        $lName = $_POST['lName'];
        $eMail = $_POST['eMail'];
        $twitter = $_POST['twitter'];
        $linkedIn = $_POST['linkedIn'];

        $concat = $statusMsg ? " AND " : "";
        $insert = $conn->query("UPDATE loginuser SET userName='" . $userName . "', eMail='" . $eMail . "', firstName='" . $fName . "', 	lastName='" . $lName . "', twitterURL='" . $twitter . "', linkedInURL='" . $linkedIn . "' where ID = " . $_SESSION["userID"]);
        if ($insert) {
            $statusMsg = $statusMsg . " " . $concat . " The profile has been updated successfully.";
        } else {
            $statusMsg = $statusMsg . " " . $concat . " Profile update failed, please try again.";
        }
    }
    mysqli_close($conn);
    echo $statusMsg;
    // $parent_dir = dirname($_SERVER['SCRIPT_NAME'], 2) . '/';
    // header("Location: ".$parent_dir. "ServerSideTours/profile.php");
    // header("HTTP/1.1 301 Moved Permanently");
    header("Location: ../profile.php");
    exit();
    // Display status message
}

updateProfile();
