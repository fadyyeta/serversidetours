<script>
    function login() {
        $.ajax({
            url: 'php/loginFunction.php',
            type: 'POST',
            data: $('form').serialize(),
            success: function(result) {
                if (result) {
                    window.location.href = (result == "registered") ? "profile.php" : "signup.php";
                } else {
                    $('#loginIncorrect').attr('display', 'inline-block');
                    return false;
                }
            }
        });
    }
</script>
<?php
function loginModal()
{
    $loginForm = "
            <div class='modal fade tg-login-lightbox' tabindex='-1' role='dialog'>
                <div class='tg-lightbox'>
                    <div class='tg-lightbox-content'>
                        <h2>Login</h2>
                        <form method='POST' action='#'>
                            <fieldset>
                                <div class='formgroup'>
                                    <input type='text' name='userName' class='form-control' placeholder='username'>
                                </div>
                                <div class='formgroup' style='display:none;'>
                                    <input type='email' name='eMail' class='form-control' placeholder='Email'>
                                </div> 
                                <div class='formgroup'>
                                    <input type='password' name='pass' class='form-control' placeholder='Password'>
                                </div>
                                <div class='formgroup' id='loginIncorrect' style='display:none;'>
                                    <span class='alert alert-danger'>Sorry you have to add valid username and password.</span>
                                </div>
                                <div class='formgroup'>
                                    <button class='tg-btn tg-btn-lg' name='submit' type='submit' onclick='login();'>login now</button>
                                </div>
                                <div class='formgroup'>
                                    <span class='tg-note'>Don't have an account? <a href='signUp.php'>SignUp</a></span>
                                </div>
                                <div class='tg-signupwith'>
                                    <div class='tg-heading-border'>
                                        <h3>OR signup with</h3>
                                    </div>
                                    <button class='tg-btn tg-btn-lg tg-facebook' type='submit'>Facebook</button>
                                    <button class='tg-btn tg-btn-lg tg-twitter' type='submit'>Twitter</button>
                                    <button class='tg-btn tg-btn-lg tg-googleplus' type='submit'>Google+</button>
                                    <button class='tg-btn tg-btn-lg tg-linkedin' type='submit'>LinkedIn</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        ";
    echo $loginForm;
}
