<?php
function headInclude($title)
{
    $headInclude = "
            <meta charset='utf-8' />
            <meta http-equiv='X-UA-Compatible' content='IE=edge' />
            <title>Travel Scope" . $title . "</title>
            <meta name='description' content='' />
            <meta name='viewport' content='width=device-width, initial-scale=1' />
            <link rel='apple-touch-icon' href='apple-touch-icon.png' />
            <link rel='stylesheet' href='css/bootstrap.min.css' />
            <link rel='stylesheet' href='css/normalize.css' />
            <link rel='stylesheet' href='css/font-awesome.min.css' />
            <link rel='stylesheet' href='css/flaticon.css' />
            <link rel='stylesheet' href='css/owl.theme.css' />
            <link rel='stylesheet' href='css/owl.carousel.css' />
            <link rel='stylesheet' href='css/jquery-ui.css' />
            <link rel='stylesheet' href='css/swiper.css' />
            <link rel='stylesheet' href='css/prettyPhoto.css' />
            <link rel='stylesheet' href='css/main.css' />
            <link rel='stylesheet' href='css/transitions.css' />
            <link rel='stylesheet' href='css/color.css' />
            <link rel='stylesheet' href='css/responsive.css' />
            <script src='js/vendor/modernizr-2.8.3-respond-1.4.2.min.js'></script>
        ";
    echo $headInclude;
}
