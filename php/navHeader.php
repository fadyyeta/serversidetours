<?php
function navBar()
{
    $navBar = "
        <header id='tg-header' class='tg-header tg-haslayout'>
            <div class='container'>
                <div class='row'>
                    <div class='col-xs-12'>
                        <strong class='tg-logo'>
                            <a href='index.php'><img src='images/logo.png' alt='image description'></a>
                        </strong>
                        <div class='tg-rightarea'>
                            <nav id='tg-nav' class='tg-nav'>
                                <div class='navbar-header'>
                                    <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#tg-navigation' aria-expanded='false'>
                                        <span class='sr-only'>Toggle navigation</span>
                                        <span class='icon-bar'></span>
                                        <span class='icon-bar'></span>
                                        <span class='icon-bar'></span>
                                    </button>
                                </div>
                                <div class='collapse navbar-collapse' id='tg-navigation'>
                                    <ul>
                                        <li class='active'>
                                            <a href='index.php'>Home</a>
                                        </li>
                                        <li><a href='aboutus.php'>About</a></li>
                                        <li><a href='packages.php'>Packages</a></li>
                                        <li>
                                            <a href='blog-grid.php'>Blog</a>
                                        </li>
                                        <li><a href='contactus.php'>Contact</a></li>
                                        <li>
                                            <a href='profile.php'>Profile</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                            <a id='tg-burger-menu' class='tg-burger-menu' href='#'><i class='fa fa-navicon'></i></a>
                            <div id='tg-additional-nav' class='tg-additional-nav'>
                                <nav class='tg-add-nav'>
                                    <ul>
                                        <li><a href='contactus.php'>help</a></li>
                                        <li><a href='faq.php'>F.A.Q</a></li>
                                        <li><a href='javascript:void(0);' data-toggle='modal' data-target='.tg-login-lightbox'>Login</a></li>
                                    </ul>
                                </nav>
                                <a id='tg-add-menu' class='tg-add-menu' href='#'><i class='fa fa-close'></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    ";
    echo $navBar;
}
