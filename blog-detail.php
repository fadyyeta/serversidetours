<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->

<!-- Mirrored from 786themes.net/html/travel-on/blog-detail.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Feb 2020 13:58:05 GMT -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BootStrap HTML5 CSS3 Theme</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flaticon.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/swiper.css">
	<link rel="stylesheet" href="css/prettyPhoto.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/transitions.css">
	<link rel="stylesheet" href="css/color.css">
	<link rel="stylesheet" href="css/responsive.css">
	<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="wrapper" class="tg-haslayout">
		<!--************************************
				Header Start
		*************************************-->
		<header id="tg-header" class="tg-header tg-haslayout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<strong class="tg-logo">
							<a href="index.html"><img src="images/logo.png" alt="image description"></a>
						</strong>
						<div class="tg-rightarea">
							<nav id="tg-nav" class="tg-nav">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tg-navigation" aria-expanded="false">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
								<div class="collapse navbar-collapse" id="tg-navigation">
									<ul>
										<li>
											<a href="#">Home</a>
											<ul>
												<li><a href="index.html">Home1</a></li>
												<li><a href="index-versiontwo.html">Home2</a></li>
											</ul>
										</li>
										<li><a href="aboutus.html">About</a></li>
										<li><a href="packages.html">Packages</a></li>
										<li class="active">
											<a href="#">Blog</a>
											<ul>
												<li><a href="blog-list.html">blog list</a></li>
												<li><a href="blog-grid.html">blog grid</a></li>
												<li class="active"><a href="blog-detail.html">blog detail</a></li>
											</ul>
										</li>
										<li><a href="contactus.html">Contact</a></li>
										<li>
											<a href="#">Pages</a>
											<ul>
												<li><a href="search-detail.html">search detail</a></li>
												<li><a href="search-result.html">search result</a></li>
												<li><a href="faq.html">faq</a></li>
												<li><a href="gallary.html">gallary</a></li>
												<li><a href="#">quick estimate</a></li>
												<li><a href="404.html">404</a></li>
												<li><a href="comming-soon.html">comming soon</a></li>
											</ul>
										</li>
									</ul>
								</div>
							</nav>
							<a id="tg-burger-menu" class="tg-burger-menu" href="#"><i class="fa fa-navicon"></i></a>
							<div id="tg-additional-nav" class="tg-additional-nav">
								<nav class="tg-add-nav">
									<ul>
										<li><a href="#">help</a></li>
										<li><a href="#">F.A.Q</a></li>
										<li><a href="javascript:void(0);" data-toggle="modal" data-target=".tg-login-lightbox">Login</a></li>
									</ul>
								</nav>
								<a id="tg-add-menu" class="tg-add-menu" href="#"><i class="fa fa-close"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!--************************************
				Header End
		*************************************-->
		<!--************************************
				Inner Pages Banner Start
		*************************************-->
		<div class="tg-banner tg-haslayout parallax-window" data-parallax="scroll" data-bleed="100" data-speed="0.2" data-image-src="images/parallax-bg/parallax-img-02.jpg">
			<div class="tg-banner-content">
				<div class="container">
					<ol class="tg-breadcrumb">
						<li><a href="#">Home</a></li>
						<li class="active">blog detail</li>
					</ol>
				</div>
			</div>
		</div>
		<!--************************************
				Inner Pages Banner End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="main" class="tg-haslayout tg-bgwhite">
			<!--************************************
				Blog Start
			*************************************-->
			<div class="tg-main-section tg-haslayout">
				<div class="container">
					<div class="row">
						<div id="tg-twocolumns" class="tg-twocolumns tg-haslayout">
							<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
								<div id="tg-content" class="tg-content tg-blogdetail tg-haslayout">
									<article class="tg-post tg-single-post">
										<div class="tg-heading-border">
											<h2>say no to lazy life</h2>
											<h3>by: jhon doe</h3>
										</div>
										<figure>
											<img src="images/blog/blog-detail/img-01.jpg" alt="image description">
										</figure>
										<div class="tg-description">
											<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna liqu Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis auteat irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
											<p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam uptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequiunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
											<blockquote>
												<q>“ If you “bite”, you play right into their hands ”</q>
											</blockquote>
											<p>Acepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut spiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
										</div>
									</article>
									<div class="tg-tags-shareicons tg-haslayout">
										<div class="tg-tagsbox pull-left">
											<strong>TAGS:</strong>
											<ul class="tg-tags">
												<li>
													<a class="tg-tag" href="#">Enjoy</a>
												</li>
												<li>
													<a class="tg-tag" href="#">lazy life</a>
												</li>
												<li>
													<a class="tg-tag" href="#">fly</a>
												</li>
												<li>
													<a class="tg-tag" href="#">joyful</a>
												</li>
											</ul>
										</div>
										<div class="tg-post-shareicons pull-right">
											<strong>SHARE:</strong>
											<ul class="tg-socialicon">
												<li>
													<a href="#">
														<i class="fa fa-facebook-f"></i>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-twitter"></i>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-pinterest-p"></i>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-tumblr"></i>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-google-plus"></i>
													</a>
												</li>
											</ul>
										</div>
									</div>
									<div class="tg-post-author tg-haslayout">
										<div class="tg-author-data">
											<div class="tg-heading-border">
												<h3><a href="#">About The Author</a></h3>
											</div>
											<figure class="tg-author-img">
												<a href="#">
													<img src="images/blog/blog-detail/author-img.jpg" alt="image description">
												</a>
											</figure>
											<div class="tg-author-infobox">
												<h3><a href="#">jhon doe</a></h3>
												<span>Blogger</span>
											</div>
										</div>
										<div class="tg-about-author">
											<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt utbore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ationt lamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor inrit in voluptate velit esse cillum dolore.</p>
										</div>
									</div>
									<div id="tg-post-comments" class="tg-post-comments tg-haslayout">
										<div class="tg-heading-border">
											<h3>3 Comments</h3>
										</div>
										<ul>
											<li>
												<div class="tg-comment tg-haslayout">
													<div class="tg-commend-head tg-haslayout">
														<div class="tg-commenthead-left pull-left">
															<figure class="tg-post-img">
																<img src="images/blog/blog-detail/comment/commenter-img1.jpg" alt="image description">
															</figure>
															<div class="tg-comment-data">
																<h3>Karen Memphis</h3>
																<span>Admin  /  June 06, 2015</span>
															</div>
														</div>
														<div class="tg-commenthead-right pull-right">
															<a href="#"><i class="fa fa-mail-reply"></i>REPLY</a>
														</div>
													</div>
													<div class="tg-description">
														<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quistrud citation ullamco laboris nisi ut aliquip ex ea commodo consequat aute irure dolor in repreherit.</p>
													</div>
												</div>
											</li>
											<li>
												<div class="tg-comment tg-haslayout">
													<div class="tg-commend-head tg-haslayout">
														<div class="tg-commenthead-left pull-left">
															<figure class="tg-post-img">
																<img src="images/blog/blog-detail/comment/commenter-img2.jpg" alt="image description">
															</figure>
															<div class="tg-comment-data">
																<h3>Karen Memphis</h3>
																<span>Admin  /  June 06, 2015</span>
															</div>
														</div>
														<div class="tg-commenthead-right pull-right">
															<a href="#"><i class="fa fa-mail-reply"></i>REPLY</a>
														</div>
													</div>
													<div class="tg-description">
														<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quistrud citation ullamco laboris nisi ut aliquip ex ea commodo consequat aute irure dolor in repreherit.</p>
													</div>
												</div>
											</li>
											<li>
												<div class="tg-comment tg-haslayout">
													<div class="tg-commend-head tg-haslayout">
														<div class="tg-commenthead-left pull-left">
															<figure class="tg-post-img">
																<img src="images/blog/blog-detail/comment/commenter-img3.jpg" alt="image description">
															</figure>
															<div class="tg-comment-data">
																<h3>Karen Memphis</h3>
																<span>Admin  /  June 06, 2015</span>
															</div>
														</div>
														<div class="tg-commenthead-right pull-right">
															<a href="#"><i class="fa fa-mail-reply"></i>REPLY</a>
														</div>
													</div>
													<div class="tg-description">
														<p>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quistrud citation ullamco laboris nisi ut aliquip ex ea commodo consequat aute irure dolor in repreherit.</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
									<div class="tg-post-comment-form tg-haslayout">
										<div class="tg-heading-border">
											<h3>Leave your Comment</h3>
										</div>
										<form class="tg-form-comment">
											<div class="row">
												<fieldset>
													<div class="col-sm-6">
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group">
																	<input type="text" class="form-control" placeholder="Name">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<input type="email" class="form-control" placeholder="Email">
																</div>
															</div>
															<div class="col-sm-12">
																<div class="form-group">
																	<input type="text" class="form-control" placeholder="Subject">
																</div>
															</div>
														</div>
													</div>
													<div class="col-sm-12">
														<div class="form-group">
															<textarea class="form-control" placeholder="Message"></textarea>
														</div>
														<button class="tg-btn" type="submit">send</button>
													</div>
												</fieldset>
											</div>
										</form>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
								<aside id="tg-sidebar" class="tg-sidebar tg-haslayout">
									<div class="tg-widget tg-searchwidget">
										<div class="tg-heading-border">
											<h3>Search widget</h3>
										</div>
										<form class="form-search">
											<fieldset>
												<input type="search" class="form-control" placeholder="Search Here">
												<button class="tg-btn-search fa fa-search"></button>
											</fieldset>
										</form>
									</div>
									<div class="tg-widget tg-widgetcategory">
										<div class="tg-heading-border">
											<h3>category widget</h3>
										</div>
										<ul>
											<li>
												<a href="#">About Us</a>
											</li>
											<li>
												<a href="#">Delivery Information</a>
											</li>
											<li>
												<a href="#">Terms and Conditions</a>
											</li>
											<li>
												<a href="#">Privacy Policy</a>
											</li>
											<li>
												<a href="#">Photography</a>
											</li>
											<li>
												<a href="#">Contact Us</a>
											</li>
											<li>
												<a href="#">Return Policy</a>
											</li>
										</ul>
									</div>
									<div class="tg-widget tg-blogwidget">
										<div class="tg-heading-border">
											<h3>recent post</h3>
										</div>
										<ul>
											<li class="tg-post">
												<figure class="tg-post-img">
													<img src="images/blog/sidebar/img-01.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
												<div class="tg-post-content">
													<h3><a href="#">start exploring</a></h3>
													<div class="tg-description">
														<p>This is Photoshop's version  of Lorem ipsum...</p>
													</div>
												</div>
											</li>
											<li class="tg-post">
												<figure class="tg-post-img">
													<img src="images/blog/sidebar/img-02.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
												<div class="tg-post-content">
													<h3><a href="#">life is about fun</a></h3>
													<div class="tg-description">
														<p>This is Photoshop's version  of Lorem ipsum...</p>
													</div>
												</div>
											</li>
											<li class="tg-post">
												<figure class="tg-post-img">
													<img src="images/blog/sidebar/img-03.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
												<div class="tg-post-content">
													<h3><a href="#">live like a king</a></h3>
													<div class="tg-description">
														<p>This is Photoshop's version  of Lorem ipsum...</p>
													</div>
												</div>
											</li>
											<li class="tg-post">
												<figure class="tg-post-img">
													<img src="images/blog/sidebar/img-04.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
												<div class="tg-post-content">
													<h3><a href="#">say no to lazy life</a></h3>
													<div class="tg-description">
														<p>This is Photoshop's version  of Lorem ipsum...</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
									<div class="tg-widget tg-recentposts">
										<div class="tg-heading-border">
											<h3>recent post</h3>
										</div>
										<ul>
											<li>
												<div class="tg-description">
													<p>Looking cautiously round, to ascerta in that they were not overheard, the two hags cowered nearer to the fire, and chuckled heartily. <a href="#">#Quote</a></p>
												</div>
											</li>
											<li>
												<div class="tg-description">
													<p>Looking cautiously round, to ascerta in that they were not overheard, the two hags cowered nearer to the fire, and chuckled heartily. <a href="#">#Quote</a></p>
												</div>
											</li>
										</ul>
									</div>
								</aside>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--************************************
				Blog End
			*************************************-->
		</main>
		<!--************************************
				Main End
		*************************************-->
		<!--************************************
				Footer Start
		*************************************-->
		<footer id="footer" class="tg-footer tg-haslayout">
			<div class="tg-getaquickestimate tg-haslayout tg-positionrelative">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="tg-heading-border">
								<h2>Get a quick estimate!</h2>
								<h3>Get a Proffesional help</h3>
							</div>
						</div>
						<form>
							<fieldset>
								<div>
									<span class="select">
										<select>
											<option>Travel Purpose</option>
											<option>Travel Purpose</option>
											<option>Travel Purpose</option>
										</select>
									</span>
								</div>
								<div>
									<input type="text" class="form-control" placeholder="Travel Date">
								</div>
								<div>
									<span class="select">
										<select>
											<option>Number Of People</option>
											<option>Number Of People</option>
											<option>Number Of People</option>
										</select>
									</span>
								</div>
								<div>
									<input type="email" class="form-control" placeholder="Your Email">
								</div>
								<div>
									<button type="submit" class="tg-btn tg-btn-lg">Calculate Now</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<div class="tg-footerarea">
				<div class="container">
					<div class="row">
						<div class="tg-columns tg-haslayout">
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-column">
									<strong class="tg-logo">
										<a href="index.html">
											<img src="images/logo2.png" alt="image description">
										</a>
									</strong>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sed do iuod tempor incididunt ut labore...</p>
									</div>
									<ul class="tg-socialicon">
										<li>
											<a href="#">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-pinterest-p"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-linkedin"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-tumblr"></i>
											</a>
										</li>
									</ul>
									<a class="tg-btn-more" href="#">More</a>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-6">
								<div class="tg-column">
									<div class="tg-heading-border">
										<h3>Fair Price Packages</h3>
									</div>
									<ul class="tg-pkgs">
										<li>
											<h4><a href="#">15% off <span>nights of london</span></a></h4>
											<p>Sectetur adiisicing elit, sed do eiusmod teporut...</p>
										</li>
										<li>
											<h4><a href="#">15% off <span>nights of london</span></a></h4>
											<p>Sectetur adiisicing elit, sed do eiusmod teporut...</p>
										</li>
										<li>
											<h4><a href="#">15% off <span>nights of london</span></a></h4>
											<p>Sectetur adiisicing elit, sed do eiusmod teporut...</p>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-md-2 col-sm-6 col-xs-6">
								<div class="tg-column">
									<div class="tg-heading-border">
										<h3>search for</h3>
									</div>
									<ul class="tg-links">
										<li><a href="#">FLight</a></li>
										<li><a href="#">Train</a></li>
										<li><a href="#">Bus</a></li>
										<li><a href="#">Car</a></li>
										<li><a href="#">Cruise</a></li>
										<li><a href="#">Room</a></li>
										<li><a href="#">Holiday</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-column">
									<div class="tg-heading-border">
										<h3>Gallery</h3>
									</div>
									<ul class="tg-gallery">
										<li>
											<figure>
												<img src="images/gallery/thumb/img-01.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-01.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
										<li>
											<figure>
												<img src="images/gallery/thumb/img-02.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-02.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
										<li>
											<figure>
												<img src="images/gallery/thumb/img-03.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-03.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
										<li>
											<figure>
												<img src="images/gallery/thumb/img-04.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-04.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
										<li>
											<figure>
												<img src="images/gallery/thumb/img-05.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-05.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
										<li>
											<figure>
												<img src="images/gallery/thumb/img-06.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-06.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tg-copyright">
				<div class="container">
					<p>&copy; 2015 | All Rights Reserved</p>
				</div>
			</div>
		</footer>
		<!--************************************
				Footer End
		*************************************-->
	</div>
	<!--************************************
			Wrapper End
	*************************************-->
	<!--************************************
			Light Box Start
	*************************************-->
	<div class="modal fade tg-login-lightbox" tabindex="-1" role="dialog">
		<div class="tg-lightbox">
			<div class="tg-lightbox-content">
				<h2>signup</h2>
				<form>
					<fieldset>
						<div class="formgroup">
							<input type="text" class="form-control" placeholder="username">
						</div>
						<div class="formgroup">
							<input type="text" class="form-control" placeholder="Email">
						</div>
						<div class="formgroup">
							<i class="fa fa-exclamation-circle"></i><span>We will email you your password.</span>
						</div>
						<div class="formgroup">
							<button class="tg-btn tg-btn-lg" type="submit">login now</button>
						</div>
						<div class="formgroup">
							<span class="tg-note">Already have an account? <a href="#">Login</a></span>
						</div>
						<div class="tg-signupwith">
							<div class="tg-heading-border">
								<h3>signup with</h3>
							</div>
							<button class="tg-btn tg-btn-lg tg-facebook" type="submit">Facebook</button>
							<button class="tg-btn tg-btn-lg tg-twitter" type="submit">Twitter</button>
							<button class="tg-btn tg-btn-lg tg-googleplus" type="submit">Google+</button>
							<button class="tg-btn tg-btn-lg tg-linkedin" type="submit">LinkedIn</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<!--************************************
			Light Box End
	*************************************-->
	<script src="js/vendor/jquery-library.js"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="js/jquery-ui.js"></script>
	<script src="js/swiper.js"></script>
	<script src="js/owl.carousel.js"></script>
	<script src="js/prettyPhoto.js"></script>
	<script src="js/parallax.js"></script>
	<script src="js/appear.js"></script>
	<script src="js/countTo.js"></script>
	<script src="js/gmap3.min.js"></script>
<script src="js/isotope.pkgd.js"></script>
<script src="js/walkway.js"></script>
	<script src="js/main.js"></script>
</body>

<!-- Mirrored from 786themes.net/html/travel-on/blog-detail.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Feb 2020 13:58:08 GMT -->
</html>