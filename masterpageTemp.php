<?php
//session_start();
require_once("php/includeHead.php");
require_once("php/includeScript.php");
require_once("php/db.php");
require_once("php/navHeader.php");
require_once("php/footer.php");
require_once("php/login.php");

$packagesFooter = ReadGeneral('packages', '*', '', 'LIMIT 3');
$galleryFooter = ReadGeneral('footergallery', '*', '', 'LIMIT 6');

?>
<!DOCTYPE html>

<head>
    <?php headInclude(' | Register') ?>
    <script>
        $(document).ready(function() {
            $('#loginIncorrect').hide();
        });
    </script>
</head>

<body>

    <div id="wrapper" class="tg-haslayout">
        <?php navBar(); ?>
        <div class="tg-banner tg-haslayout parallax-window" data-parallax="scroll" data-bleed="100" data-speed="0.2" data-image-src="images/parallax-bg/parallax-img-02.jpg">

        </div>
        <main id="main" class="tg-haslayout">
        </main>
        <?php footer($packagesFooter, $galleryFooter); ?>
    </div>
    <?php loginModal(); ?>

    <?php scriptInclude(); ?>
</body>

</html>