<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->

<!-- Mirrored from 786themes.net/html/travel-on/search-result.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Feb 2020 13:58:10 GMT -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BootStrap HTML5 CSS3 Theme</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/flaticon.css">
	<link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/swiper.css">
	<link rel="stylesheet" href="css/jquery-ui.css">
	<link rel="stylesheet" href="css/prettyPhoto.css">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/transitions.css">
	<link rel="stylesheet" href="css/color.css">
	<link rel="stylesheet" href="css/responsive.css">
	<script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="wrapper" class="tg-haslayout">
		<!--************************************
				Header Start
		*************************************-->
		<header id="tg-header" class="tg-header tg-haslayout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<strong class="tg-logo">
							<a href="index.html"><img src="images/logo.png" alt="image description"></a>
						</strong>
						<div class="tg-rightarea">
							<nav id="tg-nav" class="tg-nav">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tg-navigation" aria-expanded="false">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
								<div class="collapse navbar-collapse" id="tg-navigation">
									<ul>
										<li>
											<a href="#">Home</a>
											<ul>
												<li><a href="index.html">Home1</a></li>
												<li><a href="index-versiontwo.html">Home2</a></li>
											</ul>
										</li>
										<li><a href="aboutus.html">About</a></li>
										<li><a href="packages.html">Packages</a></li>
										<li>
											<a href="#">Blog</a>
											<ul>
												<li><a href="blog-list.html">blog list</a></li>
												<li><a href="blog-grid.html">blog grid</a></li>
												<li><a href="blog-detail.html">blog detail</a></li>
											</ul>
										</li>
										<li><a href="contactus.html">Contact</a></li>
										<li class="active">
											<a href="#">Pages</a>
											<ul>
												<li><a href="search-detail.html">search detail</a></li>
												<li class="active"><a href="search-result.html">search result</a></li>
												<li><a href="faq.html">faq</a></li>
												<li><a href="gallary.html">gallary</a></li>
												<li><a href="#">quick estimate</a></li>
												<li><a href="404.html">404</a></li>
												<li><a href="comming-soon.html">comming soon</a></li>
											</ul>
										</li>
									</ul>
								</div>
							</nav>
							<a id="tg-burger-menu" class="tg-burger-menu" href="#"><i class="fa fa-navicon"></i></a>
							<div id="tg-additional-nav" class="tg-additional-nav">
								<nav class="tg-add-nav">
									<ul>
										<li><a href="#">help</a></li>
										<li><a href="#">F.A.Q</a></li>
										<li><a href="javascript:void(0);" data-toggle="modal" data-target=".tg-login-lightbox">Login</a></li>
									</ul>
								</nav>
								<a id="tg-add-menu" class="tg-add-menu" href="#"><i class="fa fa-close"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!--************************************
				Header End
		*************************************-->
		<!--************************************
				Inner Pages Banner Start
		*************************************-->
		<div class="tg-banner tg-haslayout parallax-window" data-parallax="scroll" data-bleed="100" data-speed="0.2" data-image-src="images/parallax-bg/parallax-img-02.jpg">
			<div class="tg-banner-content">
				<div class="container">
					<ol class="tg-breadcrumb">
						<li><a href="#">Home</a></li>
						<li class="active">search result grid</li>
					</ol>
				</div>
			</div>
		</div>
		<!--************************************
				Inner Pages Banner End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="main" class="tg-haslayout">
			<div class="tg-main-section tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
							<div id="tg-content" class="tg-content">
								<div class="tg-page-head">
									<h1>search result</h1>
									<h2>37 result found</h2>
								</div>
								<ul class="tg-view-type">
									<li class="active">
										<a href="tg-grid-view.html">
											<i class="fa fa-th-large"></i>
										</a>
									</li>
									<li>
										<a href="tg-list-view.html">
											<i class="fa fa-th-list"></i>
										</a>
									</li>
								</ul>
								<div class="tg-search-result tg-grid-view">
									<article class="tg-result-item">
										<div class="row">
											<div class="col-sm-6">
												<figure>
													<img src="images/search/img-01.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
											</div>
											<div class="col-sm-6">
												<div class="tg-result-content">
													<div class="tg-title">
														<h3><a href="#">selmore Airline</a></h3>
													</div>
													<div class="tg-description">
														<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt utna labore et dolore magna aliqua enimad minim.</p>
													</div>
													<ul>
														<li><a href="#">Aliquip ex ea commodo</a></li>
														<li><a href="#">Consequat suis aute</a></li>
														<li><a href="#">Reprehenderit in voluptate</a></li>
													</ul>
													<span class="tg-result-price">
														<span>price: </span>
														<span>$139.00</span>
													</span>
												</div>
											</div>
										</div>
									</article>
									<article class="tg-result-item">
										<div class="row">
											<div class="col-sm-6">
												<figure>
													<img src="images/search/img-02.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
											</div>
											<div class="col-sm-6">
												<div class="tg-result-content">
													<div class="tg-title">
														<h3><a href="#">selmore Airline</a></h3>
													</div>
													<div class="tg-description">
														<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt utna labore et dolore magna aliqua enimad minim.</p>
													</div>
													<ul>
														<li><a href="#">Aliquip ex ea commodo</a></li>
														<li><a href="#">Consequat suis aute</a></li>
														<li><a href="#">Reprehenderit in voluptate</a></li>
													</ul>
													<span class="tg-result-price">
														<span>price: </span>
														<span>$139.00</span>
													</span>
												</div>
											</div>
										</div>
									</article>
									<article class="tg-result-item">
										<div class="row">
											<div class="col-sm-6">
												<figure>
													<img src="images/search/img-03.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
											</div>
											<div class="col-sm-6">
												<div class="tg-result-content">
													<div class="tg-title">
														<h3><a href="#">selmore Airline</a></h3>
													</div>
													<div class="tg-description">
														<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt utna labore et dolore magna aliqua enimad minim.</p>
													</div>
													<ul>
														<li><a href="#">Aliquip ex ea commodo</a></li>
														<li><a href="#">Consequat suis aute</a></li>
														<li><a href="#">Reprehenderit in voluptate</a></li>
													</ul>
													<span class="tg-result-price">
														<span>price: </span>
														<span>$139.00</span>
													</span>
												</div>
											</div>
										</div>
									</article>
									<article class="tg-result-item">
										<div class="row">
											<div class="col-sm-6">
												<figure>
													<img src="images/search/img-04.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
											</div>
											<div class="col-sm-6">
												<div class="tg-result-content">
													<div class="tg-title">
														<h3><a href="#">selmore Airline</a></h3>
													</div>
													<div class="tg-description">
														<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt utna labore et dolore magna aliqua enimad minim.</p>
													</div>
													<ul>
														<li><a href="#">Aliquip ex ea commodo</a></li>
														<li><a href="#">Consequat suis aute</a></li>
														<li><a href="#">Reprehenderit in voluptate</a></li>
													</ul>
													<span class="tg-result-price">
														<span>price: </span>
														<span>$139.00</span>
													</span>
												</div>
											</div>
										</div>
									</article>
								</div>
								<div class="tg-pagination tg-haslayout pull-right">
									<ul>
										<li>
											<a href="#"><i class="fa fa-angle-left"></i></a>
										</li>
										<li class="active">
											<a href="#">1</a>
										</li>
										<li>
											<a href="#">2</a>
										</li>
										<li>
											<a href="#">3</a>
										</li>
										<li>
											<a href="#">4</a>
										</li>
										<li>
											<a href="#">5</a>
										</li>
										<li>
											<a href="#">6</a>
										</li>
										<li>
											<a href="#"><i class="fa fa-angle-right"></i></a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
							<aside id="tg-sidebar" class="tg-sidebar tg-haslayout">
								<form class="tg-haslayout">
									<fieldset>
										<div class="tg-widget tg-pricewidget tg-haslayout">
											<div class="tg-title">
												<h3>price</h3>
											</div>
											<span class="select">
												<select>
													<option>300</option>
													<option>500</option>
													<option>150</option>
												</select>
											</span>
										</div>
										<div class="tg-widget tg-currencywidget tg-haslayout">
											<div class="tg-title">
												<h3>currency</h3>
											</div>
											<ul>
												<li>
													<label>
														<input id="tg-check" type="checkbox">
														<em>EUR</em>
													</label>
												</li>
												<li>
													<label>
														<input type="checkbox">
														<em>USD</em>
													</label>
												</li>
												<li>
													<label>
														<input type="checkbox">
														<em>AED</em>
													</label>
												</li>
												<li>
													<label>
														<input type="checkbox">
														<em>GBP</em>
													</label>
												</li>
												<li>
													<label>
														<input type="checkbox">
														<em>INR</em>
													</label>
												</li>
												<li>
													<label>
														<input type="checkbox">
														<em>CAD</em>
													</label>
												</li>
											</ul>
										</div>
										<div class="tg-widget tg-radiuswidget tg-haslayout">
											<div class="tg-title">
												<h3>radius</h3>
											</div>
											<div class="tg-raidus">
												<p>
<!--													<label for="amount">Donation amount ($50 increments):</label>-->
													<input type="text" id="amount" readonly style="border:0; color:#f6931f; font-weight:bold;">
												</p>
												<div id="tg-rangeradius" class="tg-rangeradius"></div>
											</div>
										</div>
										<div class="tg-widget tg-currencywidget tg-countrywidget tg-haslayout">
											<div class="tg-title">
												<h3>currency</h3>
											</div>
											<ul>
												<li>
													<label>
														<input type="checkbox">
														<em>Selmore</em>
													</label>
												</li>
												<li>
													<label>
														<input type="checkbox">
														<em>All</em>
													</label>
												</li>
												<li>
													<label>
														<input type="checkbox">
														<em>Quantum</em>
													</label>
												</li>
												<li>
													<label>
														<input type="checkbox">
														<em>Fly High</em>
													</label>
												</li>
												<li>
													<label>
														<input type="checkbox">
														<em>Speed</em>
													</label>
												</li>
												<li>
													<label>
														<input type="checkbox">
														<em>Rhino</em>
													</label>
												</li>
											</ul>
										</div>
									</fieldset>
								</form>
							</aside>
						</div>
					</div>
				</div>
			</div>
			<!--************************************
					Testimonials Start
			*************************************-->
			<section class="tg-main-section tg-haslayout tg-bglight">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12 pull-right">
							<div class="tg-testimonials-box">
								<div class="tg-heading-border">
									<h2>customer feedback</h2>
									<h3>our testimonials</h3>
								</div>
								<div class="tg-testimonials-sliderbox">
									<div class="col-sm-6">
										<div id="tg-testimonials-message-slider" class="tg-testimonials-message-slider tg-testimonials-message">
											<div class="item">
												<div class="tg-heading-border">
													<h4>Jennifer Doe &amp; family</h4>
												</div>
												<div class="tg-description">
													<blockquote>
														<q>Adipisicing elit, sed do eiusmod tempor incididaut labore etolore magna aliqua. Ut enim ad minim am quis nostrud exercitation ullamco.</q>
													</blockquote>
												</div>
											</div>
											<div class="item">
												<div class="tg-heading-border">
													<h4>Jennifer Doe &amp; family</h4>
												</div>
												<div class="tg-description">
													<blockquote>
														<q>Adipisicing elit, sed do eiusmod tempor incididaut labore etolore magna aliqua. Ut enim ad minim am quis nostrud exercitation ullamco.</q>
													</blockquote>
												</div>
											</div>
											<div class="item">
												<div class="tg-heading-border">
													<h4>Jennifer Doe &amp; family</h4>
												</div>
												<div class="tg-description">
													<blockquote>
														<q>Adipisicing elit, sed do eiusmod tempor incididaut labore etolore magna aliqua. Ut enim ad minim am quis nostrud exercitation ullamco.</q>
													</blockquote>
												</div>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div id="tg-testimonials-pagger-slider" class="tg-testimonials-pagger-slider tg-testimonials-pagger">
											<div class="item">
												<img src="images/testimonials/thumb-01.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<i class="flaticon-plus79"></i>
												</div>
											</div>
											<div class="item">
												<img src="images/testimonials/thumb-02.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<i class="flaticon-plus79"></i>
												</div>
											</div>
											<div class="item">
												<img src="images/testimonials/thumb-03.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<i class="flaticon-plus79"></i>
												</div>
											</div>
										</div>
										<a class="tg-btn-viewall" href="#">view all</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-12 col-xs-12 pull-left">
							<figure class="tg-imgbox">
								<img src="images/img-01.jpg" alt="image description">
							</figure>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Testimonials End
			*************************************-->
			<!--************************************
					Packages Start
			*************************************-->
			<section class="tg-main-section tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 col-xs-12">
							<div class="tg-section-head">
								<div class="tg-section-heading">
									<h2>fair price packages</h2>
								</div>
								<div class="tg-description">
									<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								</div>
							</div>
						</div>
					</div>
					<div id="tg-packages-slider" class="tg-packages-slider tg-packages">
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-01.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>15%</h3>
									</div>
									<h4><a href="#">Nights of london</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-02.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>23%</h3>
									</div>
									<h4><a href="#">family fun in malaysia</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-03.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>10%</h3>
									</div>
									<h4><a href="#">charm in dubai</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-04.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>37%</h3>
									</div>
									<h4><a href="#">journey of hope</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-01.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>15%</h3>
									</div>
									<h4><a href="#">Nights of london</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-02.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>23%</h3>
									</div>
									<h4><a href="#">family fun in malaysia</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-03.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>10%</h3>
									</div>
									<h4><a href="#">charm in dubai</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-04.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>37%</h3>
									</div>
									<h4><a href="#">journey of hope</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-01.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>15%</h3>
									</div>
									<h4><a href="#">Nights of london</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-02.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>23%</h3>
									</div>
									<h4><a href="#">family fun in malaysia</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-03.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>10%</h3>
									</div>
									<h4><a href="#">charm in dubai</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-04.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>37%</h3>
									</div>
									<h4><a href="#">journey of hope</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Packages End
			*************************************-->
		</main>
		<!--************************************
				Main End
		*************************************-->
		<!--************************************
				Footer Start
		*************************************-->
		<footer id="footer" class="tg-footer tg-haslayout">
			<div class="tg-getaquickestimate tg-haslayout tg-positionrelative">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="tg-heading-border">
								<h2>Get a quick estimate!</h2>
								<h3>Get a Proffesional help</h3>
							</div>
						</div>
						<form>
							<fieldset>
								<div>
									<span class="select">
										<select>
											<option>Travel Purpose</option>
											<option>Travel Purpose</option>
											<option>Travel Purpose</option>
										</select>
									</span>
								</div>
								<div>
									<input type="text" class="form-control" placeholder="Travel Date">
								</div>
								<div>
									<span class="select">
										<select>
											<option>Number Of People</option>
											<option>Number Of People</option>
											<option>Number Of People</option>
										</select>
									</span>
								</div>
								<div>
									<input type="email" class="form-control" placeholder="Your Email">
								</div>
								<div>
									<button type="submit" class="tg-btn tg-btn-lg">Calculate Now</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
			<div class="tg-footerarea">
				<div class="container">
					<div class="row">
						<div class="tg-columns tg-haslayout">
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-column">
									<strong class="tg-logo">
										<a href="index.html">
											<img src="images/logo2.png" alt="image description">
										</a>
									</strong>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sed do iuod tempor incididunt ut labore...</p>
									</div>
									<ul class="tg-socialicon">
										<li>
											<a href="#">
												<i class="fa fa-facebook"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-twitter"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-pinterest-p"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-linkedin"></i>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-tumblr"></i>
											</a>
										</li>
									</ul>
									<a class="tg-btn-more" href="#">More</a>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-6">
								<div class="tg-column">
									<div class="tg-heading-border">
										<h3>Fair Price Packages</h3>
									</div>
									<ul class="tg-pkgs">
										<li>
											<h4><a href="#">15% off <span>nights of london</span></a></h4>
											<p>Sectetur adiisicing elit, sed do eiusmod teporut...</p>
										</li>
										<li>
											<h4><a href="#">15% off <span>nights of london</span></a></h4>
											<p>Sectetur adiisicing elit, sed do eiusmod teporut...</p>
										</li>
										<li>
											<h4><a href="#">15% off <span>nights of london</span></a></h4>
											<p>Sectetur adiisicing elit, sed do eiusmod teporut...</p>
										</li>
									</ul>
								</div>
							</div>
							<div class="col-md-2 col-sm-6 col-xs-6">
								<div class="tg-column">
									<div class="tg-heading-border">
										<h3>search for</h3>
									</div>
									<ul class="tg-links">
										<li><a href="#">FLight</a></li>
										<li><a href="#">Train</a></li>
										<li><a href="#">Bus</a></li>
										<li><a href="#">Car</a></li>
										<li><a href="#">Cruise</a></li>
										<li><a href="#">Room</a></li>
										<li><a href="#">Holiday</a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-3 col-sm-6 col-xs-6">
								<div class="tg-column">
									<div class="tg-heading-border">
										<h3>Gallery</h3>
									</div>
									<ul class="tg-gallery">
										<li>
											<figure>
												<img src="images/gallery/thumb/img-01.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-01.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
										<li>
											<figure>
												<img src="images/gallery/thumb/img-02.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-02.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
										<li>
											<figure>
												<img src="images/gallery/thumb/img-03.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-03.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
										<li>
											<figure>
												<img src="images/gallery/thumb/img-04.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-04.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
										<li>
											<figure>
												<img src="images/gallery/thumb/img-05.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-05.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
										<li>
											<figure>
												<img src="images/gallery/thumb/img-06.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="images/gallery/thumb/img-06.jpg" data-rel="prettyPhoto[gallery]"><i class="flaticon-plus79"></i></a>
												</div>
											</figure>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tg-copyright">
				<div class="container">
					<p>&copy; 2015 | All Rights Reserved</p>
				</div>
			</div>
		</footer>
		<!--************************************
				Footer End
		*************************************-->
	</div>
	<!--************************************
			Wrapper End
	*************************************-->
	<!--************************************
			Light Box Start
	*************************************-->
	<div class="modal fade tg-login-lightbox" tabindex="-1" role="dialog">
		<div class="tg-lightbox">
			<div class="tg-lightbox-content">
				<h2>signup</h2>
				<form>
					<fieldset>
						<div class="formgroup">
							<input type="text" class="form-control" placeholder="username">
						</div>
						<div class="formgroup">
							<input type="text" class="form-control" placeholder="Email">
						</div>
						<div class="formgroup">
							<i class="fa fa-exclamation-circle"></i><span>We will email you your password.</span>
						</div>
						<div class="formgroup">
							<button class="tg-btn tg-btn-lg" type="submit">login now</button>
						</div>
						<div class="formgroup">
							<span class="tg-note">Already have an account? <a href="#">Login</a></span>
						</div>
						<div class="tg-signupwith">
							<div class="tg-heading-border">
								<h3>signup with</h3>
							</div>
							<button class="tg-btn tg-btn-lg tg-facebook" type="submit">Facebook</button>
							<button class="tg-btn tg-btn-lg tg-twitter" type="submit">Twitter</button>
							<button class="tg-btn tg-btn-lg tg-googleplus" type="submit">Google+</button>
							<button class="tg-btn tg-btn-lg tg-linkedin" type="submit">LinkedIn</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<!--************************************
			Light Box End
	*************************************-->
	<script src="js/vendor/jquery-library.js"></script>
	<script src="js/vendor/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="js/jquery-ui.js"></script>
	<script src="js/swiper.js"></script>
	<script src="js/owl.carousel.js"></script>
	<script src="js/prettyPhoto.js"></script>
	<script src="js/parallax.js"></script>
	<script src="js/appear.js"></script>
	<script src="js/countTo.js"></script>
	<script src="js/gmap3.min.js"></script>
<script src="js/isotope.pkgd.js"></script>
<script src="js/walkway.js"></script>
	<script src="js/main.js"></script>
</body>

<!-- Mirrored from 786themes.net/html/travel-on/search-result.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Feb 2020 13:58:15 GMT -->
</html>