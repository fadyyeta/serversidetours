<?php
session_start();
require_once("php/includeHead.php");
require_once("php/includeScript.php");
require_once("php/db.php");
require_once("php/navHeader.php");
require_once("php/footer.php");
// require_once("php/profileUpdate.php");

$packagesFooter = ReadGeneral('packages', '*', '', 'LIMIT 3');
$galleryFooter = ReadGeneral('footergallery', '*', '', 'LIMIT 6');
if ($_SESSION['userID']) {
    $profileData = ReadGeneral('loginuser', '*', '', 'where ID = ' . $_SESSION['userID']);
    $profileRow = $profileData[0];
    $userName = $profileRow['userName'];
    $firstName = $profileRow['firstName'];
    $lastName = $profileRow['lastName'];
    $fullName = $firstName . ' ' . $lastName;
    $nameSalutation = trim($fullName) ? $fullName : $userName;
    $imgURL = $profileRow['imgURL'];
    $ppUrl = $imgURL ? 'uploads/' . $imgURL : 'https://bootdey.com/img/Content/avatar/avatar1.png';
    $email = $profileRow['eMail'];
    $twitter = $profileRow['twitterURL'];
    $linkedIn = $profileRow['linkedInURL'];
} else {
    header('Location: index.php');
}

?>
<!DOCTYPE html>

<head>
    <?php headInclude(' | Profile') ?>
    <link rel="stylesheet" href="css/profile.css" />
</head>

<body>

    <div id="wrapper" class="tg-haslayout">
        <?php navBar(); ?>
        <div class="tg-banner tg-haslayout parallax-window" data-parallax="scroll" data-bleed="100" data-speed="0.2" data-image-src="images/parallax-bg/parallax-img-02.jpg">

        </div>
        <main id="main" class="tg-haslayout">
            <div class="container">
                <div class="view-account">
                    <section class="module">
                        <div class="module-inner">
                            <div class="side-bar">
                                <div class="user-info">
                                    <img class="img-profile img-circle img-responsive center-block" src="<?php echo htmlspecialchars($ppUrl); ?>" alt="">
                                    <ul class="meta list list-unstyled">
                                        <li class="name"><?php echo htmlspecialchars($nameSalutation); ?>
                                        </li>
                                        <li class="email"><a href="#"><?php echo htmlspecialchars($email); ?></a></li>
                                    </ul>
                                </div>
                                <nav class="side-menu">
                                    <ul class="nav">
                                        <li class="profileRead active"><a href="#profile"><span class="fa fa-user"></span> Profile</a></li>
                                        <li class="profileEdit"><a href=" #"><span class="fa fa-edit"></span> Edit Profile</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="content-panel">
                                <!-- <h2 class="title">Profile<span class="pro-label label label-warning">PRO</span></h2> -->
                                <form class="form-horizontal" action="php/profileUpdate.php" method="post" enctype="multipart/form-data">
                                    <fieldset class="fieldset">
                                        <h3 class="fieldset-title">Personal Info</h3>
                                        <div class="form-group avatar">
                                            <figure class="figure col-md-2 col-sm-3 col-xs-12">
                                                <img class="img-rounded img-responsive" src="<?php echo htmlspecialchars($ppUrl); ?>" alt="">
                                            </figure>
                                            <div class="form-inline col-md-10 col-sm-9 col-xs-12">
                                                <input type="file" name="file" class="file-uploader pull-left">
                                                <button type="submit" name="uploadImg" class="btn btn-sm btn-default-alt pull-left">Update Image</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 col-sm-3 col-xs-12 control-label">User Name</label>
                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                <input type="text" name="userName" class="form-control" value="<?php echo htmlspecialchars($userName); ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 col-sm-3 col-xs-12 control-label">First Name</label>
                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                <input type="text" name="fName" class="form-control" value="<?php echo htmlspecialchars($firstName); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 col-sm-3 col-xs-12 control-label">Last Name</label>
                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                <input type="text" name="lName" class="form-control" value="<?php echo htmlspecialchars($lastName); ?>">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset class="fieldset">
                                        <h3 class="fieldset-title">Contact Info</h3>
                                        <div class="form-group">
                                            <label class="col-md-2  col-sm-3 col-xs-12 control-label">Email</label>
                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                <input type="email" name="eMail" class="form-control" value="<?php echo htmlspecialchars($email); ?>">
                                                <p class="help-block">This is the email </p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2  col-sm-3 col-xs-12 control-label">Twitter</label>
                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                <input type="text" name="twitter" class="form-control" value="<?php echo htmlspecialchars($twitter); ?>">
                                                <p class="help-block">Your twitter username</p>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2  col-sm-3 col-xs-12 control-label">Linkedin</label>
                                            <div class="col-md-10 col-sm-9 col-xs-12">
                                                <input type="text" name="linkedIn" class="form-control" value="<?php echo htmlspecialchars($linkedIn); ?>">
                                                <p class="help-block">eg. https://www.linkedin.com/in/<?php echo htmlspecialchars($userName); ?></p>
                                            </div>
                                        </div>
                                    </fieldset>
                                    <hr>
                                    <div class="form-group">
                                        <div class="col-md-10 col-sm-9 col-xs-12 col-md-push-2 col-sm-push-3 col-xs-push-0">
                                            <input class="btn btn-primary" id="updateProfile" type="submit" name="updateProf" value="Update Profile">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </main>
        <?php footer($packagesFooter, $galleryFooter); ?>
    </div>
    <?php scriptInclude(); ?>
    <script>
        $(".profileEdit").click(function(e) {
            ProfileUpdate(e);
        });

        $(".profileRead").click(function(e) {
            ProfileRead(e);
        });

        function ProfileRead(e) {
            //alert('read');
            e.preventDefault();
            $(".content-panel").css({
                "opacity": "1",
                "display": "none",
            }).animate({
                opacity: 0
            }, 1500);
            $(".side-bar").css({
                "opacity": "0",
                "display": "contents",
            }).animate({
                opacity: 1
            }, 1500);
            $(".profileRead").addClass("active");
            $(".profileEdit").removeClass("active");

        }

        function ProfileUpdate(e) {
            //alert('update');
            e.preventDefault();
            $(".content-panel").css({
                "opacity": "0",
                "display": "inline-block",
            }).animate({
                opacity: 1
            }, 1500);
            $(".side-bar").css({
                "opacity": "0",
                "display": "inline-block",
            }).animate({
                opacity: 1
            }, 1500);
            $(".profileRead").removeClass("active");
            $(".profileEdit").addClass("active");

        }
    </script>
    <script>
        $("#updateProfile").click(function() {
            ProfUpdate();
        });

        function ProfUpdate() {
            $.ajax({
                url: 'php/ProfileUpdate.php',
                type: 'POST',
                data: $('form').serialize(),
                success: function(result) {
                    if (!result) {
                        return false;
                    }
                }
            });
        }
    </script>

</body>

</html>