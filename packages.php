<?php
//session_start();
require_once("php/includeHead.php");
require_once("php/includeScript.php");
require_once("php/db.php");
require_once("php/navHeader.php");
require_once("php/footer.php");
require_once("php/login.php");
$packagesFooter = ReadGeneral('packages', '*', '', 'LIMIT 3');
$galleryFooter = ReadGeneral('footergallery', '*', '', 'LIMIT 6');

?>
<!DOCTYPE html>

<head>
	<?php headInclude(' | Packages') ?>
</head>

<body>

	<div id="wrapper" class="tg-haslayout">
		<?php navBar(); ?>
		<div class="tg-banner tg-haslayout parallax-window" data-parallax="scroll" data-bleed="100" data-speed="0.2" data-image-src="images/parallax-bg/parallax-img-02.jpg">
			<div class="tg-banner-content">
				<div class="container">
					<ol class="tg-breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li class="active">packages</li>
					</ol>
				</div>
			</div>
		</div>
		<main id="main" class="tg-haslayout">
			<!--************************************
					Packages Start
			*************************************-->
			<section class="tg-main-section tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 col-xs-12">
							<div class="tg-section-head">
								<div class="tg-section-heading">
									<h2>fair price packages</h2>
								</div>
								<div class="tg-description">
									<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								</div>
							</div>
						</div>
					</div>
					<div id="tg-packages-slider" class="tg-packages-slider tg-packages tg-fullslider">
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-01.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>15%</h3>
									</div>
									<h4><a href="#">Nights of london</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-02.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>23%</h3>
									</div>
									<h4><a href="#">family fun in malaysia</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-03.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>10%</h3>
									</div>
									<h4><a href="#">charm in dubai</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-04.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>37%</h3>
									</div>
									<h4><a href="#">journey of hope</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-01.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>15%</h3>
									</div>
									<h4><a href="#">Nights of london</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-02.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>23%</h3>
									</div>
									<h4><a href="#">family fun in malaysia</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-03.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>10%</h3>
									</div>
									<h4><a href="#">charm in dubai</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-04.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>37%</h3>
									</div>
									<h4><a href="#">journey of hope</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-01.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>15%</h3>
									</div>
									<h4><a href="#">Nights of london</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-02.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>23%</h3>
									</div>
									<h4><a href="#">family fun in malaysia</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-03.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>10%</h3>
									</div>
									<h4><a href="#">charm in dubai</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
						<div class="item">
							<figure class="tg-package">
								<img src="images/packages/img-04.jpg" alt="image description">
								<figcaption>
									<div class="tg-heading-border">
										<h3>37%</h3>
									</div>
									<h4><a href="#">journey of hope</a></h4>
									<div class="tg-description">
										<p>Consectetur adipisicing elit sesmod tempor incididunt ut labore etlore magna aliquad minim.</p>
									</div>
									<a href="#"><i class="flaticon-directional27"></i></a>
								</figcaption>
							</figure>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Packages End
			*************************************-->
		</main>
		<?php footer($packagesFooter, $galleryFooter); ?>
	</div>
	<?php loginModal(); ?>

	<?php scriptInclude(); ?>
</body>

</html>