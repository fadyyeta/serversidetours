<?php
//session_start();
require_once("php/includeHead.php");
require_once("php/includeScript.php");
require_once("php/db.php");
require_once("php/navHeader.php");
require_once("php/footer.php");
require_once("php/login.php");
$packagesFooter = ReadGeneral('packages', '*', '', 'LIMIT 3');
$galleryFooter = ReadGeneral('footergallery', '*', '', 'LIMIT 6');

?>
<!DOCTYPE html>

<head>
	<?php headInclude(' | Blog') ?>
</head>

<body>

	<div id="wrapper" class="tg-haslayout">
		<?php navBar(); ?>
		<div class="tg-banner tg-haslayout parallax-window" data-parallax="scroll" data-bleed="100" data-speed="0.2" data-image-src="images/parallax-bg/parallax-img-02.jpg">
			<div class="tg-banner-content">
				<div class="container">
					<ol class="tg-breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li class="active">blog grid</li>
					</ol>
				</div>
			</div>
		</div>
		<main id="main" class="tg-haslayout">
			<!--************************************
					Blog Post Start
			*************************************-->
			<section class="tg-main-section tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 col-xs-12">
							<div class="tg-section-head">
								<div class="tg-section-heading">
									<h2>latest blog</h2>
								</div>
								<div class="tg-description">
									<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								</div>
							</div>
						</div>
						<div class="tg-bloggrid tg-haslayout">
							<div class="col-md-4 col-sm-6 col-xs-6 tg-marginbottom">
								<div class="tg-post">
									<div class="row">
										<div class="col-sm-12">
											<figure class="tg-post-img">
												<img src="images/blog/blog-grid/img-01.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="#"><i class="flaticon-directional27"></i></a>
												</div>
											</figure>
										</div>
										<div class="col-sm-12">
											<div class="tg-post-content">
												<ul class="tg-postmata">
													<li>
														<time datetime="2015-12-15 12:30"><a href="#">Feb 01, 2016</a></time>
													</li>
												</ul>
												<div class="tg-heading-border">
													<h3><a href="#">10 things todo before travel</a></h3>
												</div>
												<div class="tg-description">
													<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enimad minim veniam, quis nostrud...</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-6 tg-marginbottom">
								<div class="tg-post">
									<div class="row">
										<div class="col-sm-12">
											<figure class="tg-post-img">
												<img src="images/blog/blog-grid/img-02.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="#"><i class="flaticon-directional27"></i></a>
												</div>
											</figure>
										</div>
										<div class="col-sm-12">
											<div class="tg-post-content">
												<ul class="tg-postmata">
													<li>
														<time datetime="2015-12-15 12:30"><a href="#">Feb 01, 2016</a></time>
													</li>
												</ul>
												<div class="tg-heading-border">
													<h3><a href="#">10 things todo before travel</a></h3>
												</div>
												<div class="tg-description">
													<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enimad minim veniam, quis nostrud...</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-6 tg-marginbottom">
								<div class="tg-post">
									<div class="row">
										<div class="col-sm-12">
											<figure class="tg-post-img">
												<img src="images/blog/blog-grid/img-03.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="#"><i class="flaticon-directional27"></i></a>
												</div>
											</figure>
										</div>
										<div class="col-sm-12">
											<div class="tg-post-content">
												<ul class="tg-postmata">
													<li>
														<time datetime="2015-12-15 12:30"><a href="#">Feb 01, 2016</a></time>
													</li>
												</ul>
												<div class="tg-heading-border">
													<h3><a href="#">10 things todo before travel</a></h3>
												</div>
												<div class="tg-description">
													<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enimad minim veniam, quis nostrud...</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-6 tg-marginbottom">
								<div class="tg-post">
									<div class="row">
										<div class="col-sm-12">
											<figure class="tg-post-img">
												<img src="images/blog/blog-grid/img-04.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="#"><i class="flaticon-directional27"></i></a>
												</div>
											</figure>
										</div>
										<div class="col-sm-12">
											<div class="tg-post-content">
												<ul class="tg-postmata">
													<li>
														<time datetime="2015-12-15 12:30"><a href="#">Feb 01, 2016</a></time>
													</li>
												</ul>
												<div class="tg-heading-border">
													<h3><a href="#">10 things todo before travel</a></h3>
												</div>
												<div class="tg-description">
													<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enimad minim veniam, quis nostrud...</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-6 tg-marginbottom">
								<div class="tg-post">
									<div class="row">
										<div class="col-sm-12">
											<figure class="tg-post-img">
												<img src="images/blog/blog-grid/img-05.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="#"><i class="flaticon-directional27"></i></a>
												</div>
											</figure>
										</div>
										<div class="col-sm-12">
											<div class="tg-post-content">
												<ul class="tg-postmata">
													<li>
														<time datetime="2015-12-15 12:30"><a href="#">Feb 01, 2016</a></time>
													</li>
												</ul>
												<div class="tg-heading-border">
													<h3><a href="#">10 things todo before travel</a></h3>
												</div>
												<div class="tg-description">
													<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enimad minim veniam, quis nostrud...</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-6 tg-marginbottom">
								<div class="tg-post">
									<div class="row">
										<div class="col-sm-12">
											<figure class="tg-post-img">
												<img src="images/blog/blog-grid/img-06.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="#"><i class="flaticon-directional27"></i></a>
												</div>
											</figure>
										</div>
										<div class="col-sm-12">
											<div class="tg-post-content">
												<ul class="tg-postmata">
													<li>
														<time datetime="2015-12-15 12:30"><a href="#">Feb 01, 2016</a></time>
													</li>
												</ul>
												<div class="tg-heading-border">
													<h3><a href="#">10 things todo before travel</a></h3>
												</div>
												<div class="tg-description">
													<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enimad minim veniam, quis nostrud...</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-6">
								<div class="tg-post">
									<div class="row">
										<div class="col-sm-12">
											<figure class="tg-post-img">
												<img src="images/blog/blog-grid/img-07.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="#"><i class="flaticon-directional27"></i></a>
												</div>
											</figure>
										</div>
										<div class="col-sm-12">
											<div class="tg-post-content">
												<ul class="tg-postmata">
													<li>
														<time datetime="2015-12-15 12:30"><a href="#">Feb 01, 2016</a></time>
													</li>
												</ul>
												<div class="tg-heading-border">
													<h3><a href="#">10 things todo before travel</a></h3>
												</div>
												<div class="tg-description">
													<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enimad minim veniam, quis nostrud...</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-6">
								<div class="tg-post">
									<div class="row">
										<div class="col-sm-12">
											<figure class="tg-post-img">
												<img src="images/blog/blog-grid/img-08.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="#"><i class="flaticon-directional27"></i></a>
												</div>
											</figure>
										</div>
										<div class="col-sm-12">
											<div class="tg-post-content">
												<ul class="tg-postmata">
													<li>
														<time datetime="2015-12-15 12:30"><a href="#">Feb 01, 2016</a></time>
													</li>
												</ul>
												<div class="tg-heading-border">
													<h3><a href="#">10 things todo before travel</a></h3>
												</div>
												<div class="tg-description">
													<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enimad minim veniam, quis nostrud...</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-6">
								<div class="tg-post">
									<div class="row">
										<div class="col-sm-12">
											<figure class="tg-post-img">
												<img src="images/blog/blog-grid/img-09.jpg" alt="image description">
												<div class="tg-hover-pattran">
													<a href="#"><i class="flaticon-directional27"></i></a>
												</div>
											</figure>
										</div>
										<div class="col-sm-12">
											<div class="tg-post-content">
												<ul class="tg-postmata">
													<li>
														<time datetime="2015-12-15 12:30"><a href="#">Feb 01, 2016</a></time>
													</li>
												</ul>
												<div class="tg-heading-border">
													<h3><a href="#">10 things todo before travel</a></h3>
												</div>
												<div class="tg-description">
													<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua enimad minim veniam, quis nostrud...</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tg-pagination tg-haslayout pull-right">
							<ul>
								<li>
									<a href="#"><i class="fa fa-angle-left"></i></a>
								</li>
								<li class="active">
									<a href="#">1</a>
								</li>
								<li>
									<a href="#">2</a>
								</li>
								<li>
									<a href="#">3</a>
								</li>
								<li>
									<a href="#">4</a>
								</li>
								<li>
									<a href="#">5</a>
								</li>
								<li>
									<a href="#">6</a>
								</li>
								<li>
									<a href="#"><i class="fa fa-angle-right"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Blog Post End
			*************************************-->
		</main>

		<?php footer($packagesFooter, $galleryFooter); ?>
	</div>
	<?php loginModal(); ?>
	<?php scriptInclude(); ?>
</body>

</html>