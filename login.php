<!--************************************
                        Light Box Start
        *************************************-->
<div class="modal fade tg-login-lightbox" tabindex="-1" role="dialog">
    <div class="tg-lightbox">
        <div class="tg-lightbox-content">
            <h2>signup</h2>
            <form>
                <fieldset>
                    <div class="formgroup">
                        <input type="text" class="form-control" placeholder="username">
                    </div>
                    <div class="formgroup">
                        <input type="text" class="form-control" placeholder="Email">
                    </div>
                    <div class="formgroup">
                        <i class="fa fa-exclamation-circle"></i><span>We will email you your password.</span>
                    </div>
                    <div class="formgroup">
                        <button class="tg-btn tg-btn-lg" type="submit">login now</button>
                    </div>
                    <div class="formgroup">
                        <span class="tg-note">Already have an account? <a href="#">Login</a></span>
                    </div>
                    <div class="tg-signupwith">
                        <div class="tg-heading-border">
                            <h3>signup with</h3>
                        </div>
                        <button class="tg-btn tg-btn-lg tg-facebook" type="submit">Facebook</button>
                        <button class="tg-btn tg-btn-lg tg-twitter" type="submit">Twitter</button>
                        <button class="tg-btn tg-btn-lg tg-googleplus" type="submit">Google+</button>
                        <button class="tg-btn tg-btn-lg tg-linkedin" type="submit">LinkedIn</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<!--************************************
                            Light Box End
            *************************************-->