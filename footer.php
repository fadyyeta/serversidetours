<!--************************************
                            Footer Start
            *************************************-->
<footer id="footer" class="tg-footer tg-haslayout">
    <div class="tg-getaquickestimate tg-haslayout tg-positionrelative">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="tg-heading-border">
                        <h2>Get a quick estimate!</h2>
                        <h3>Get a Proffesional help</h3>
                    </div>
                </div>
                <form>
                    <fieldset>
                        <div>
                            <span class="select">
                                <select>
                                    <option>Travel Purpose</option>
                                    <option>Travel Purpose</option>
                                    <option>Travel Purpose</option>
                                </select>
                            </span>
                        </div>
                        <div>
                            <input type="text" class="form-control" placeholder="Travel Date">
                        </div>
                        <div>
                            <span class="select">
                                <select>
                                    <option>Number Of People</option>
                                    <option>Number Of People</option>
                                    <option>Number Of People</option>
                                </select>
                            </span>
                        </div>
                        <div>
                            <input type="email" class="form-control" placeholder="Your Email">
                        </div>
                        <div>
                            <button type="submit" class="tg-btn tg-btn-lg">Calculate Now</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    <div class="tg-footerarea">
        <div class="container">
            <div class="row">
                <div class="tg-columns tg-haslayout">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="tg-column">
                            <strong class="tg-logo">
                                <a href="index.html">
                                    <img src="images/logo.png" alt="image description">
                                </a>
                            </strong>
                            <div class="tg-description">
                                <p>Consectetur adipisicing elit sed do iuod tempor incididunt ut labore...</p>
                            </div>
                            <ul class="tg-socialicon">
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-pinterest-p"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-tumblr"></i>
                                    </a>
                                </li>
                            </ul>
                            <a class="tg-btn-more" href="#">More</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="tg-column">
                            <div class="tg-heading-border">
                                <h3>Fair Price Packages</h3>
                            </div>
                            <ul class="tg-pkgs">
                                <li>
                                    <h4><a href="#">15% off <span>nights of london</span></a></h4>
                                    <p>Sectetur adiisicing elit, sed do eiusmod teporut...</p>
                                </li>
                                <li>
                                    <h4><a href="#">15% off <span>nights of london</span></a></h4>
                                    <p>Sectetur adiisicing elit, sed do eiusmod teporut...</p>
                                </li>
                                <li>
                                    <h4><a href="#">15% off <span>nights of london</span></a></h4>
                                    <p>Sectetur adiisicing elit, sed do eiusmod teporut...</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-6">
                        <div class="tg-column">
                            <div class="tg-heading-border">
                                <h3>search for</h3>
                            </div>
                            <ul class="tg-links">
                                <li><a href="#">FLight</a></li>
                                <li><a href="#">Train</a></li>
                                <li><a href="#">Bus</a></li>
                                <li><a href="#">Car</a></li>
                                <li><a href="#">Cruise</a></li>
                                <li><a href="#">Room</a></li>
                                <li><a href="#">Holiday</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="tg-column">
                            <div class="tg-heading-border">
                                <h3>Gallery</h3>
                            </div>
                            <ul class="tg-gallery">
                                <li>
                                    <figure>
                                        <img src="images/gallery/thumb/img-01.jpg" alt="image description">
                                        <div class="tg-hover-pattran">
                                            <a href="images/gallery/thumb/img-01.jpg" data-rel="prettyPhoto[gallery]"><i
                                                    class="flaticon-plus79"></i></a>
                                        </div>
                                    </figure>
                                </li>
                                <li>
                                    <figure>
                                        <img src="images/gallery/thumb/img-02.jpg" alt="image description">
                                        <div class="tg-hover-pattran">
                                            <a href="images/gallery/thumb/img-02.jpg" data-rel="prettyPhoto[gallery]"><i
                                                    class="flaticon-plus79"></i></a>
                                        </div>
                                    </figure>
                                </li>
                                <li>
                                    <figure>
                                        <img src="images/gallery/thumb/img-03.jpg" alt="image description">
                                        <div class="tg-hover-pattran">
                                            <a href="images/gallery/thumb/img-03.jpg" data-rel="prettyPhoto[gallery]"><i
                                                    class="flaticon-plus79"></i></a>
                                        </div>
                                    </figure>
                                </li>
                                <li>
                                    <figure>
                                        <img src="images/gallery/thumb/img-04.jpg" alt="image description">
                                        <div class="tg-hover-pattran">
                                            <a href="images/gallery/thumb/img-04.jpg" data-rel="prettyPhoto[gallery]"><i
                                                    class="flaticon-plus79"></i></a>
                                        </div>
                                    </figure>
                                </li>
                                <li>
                                    <figure>
                                        <img src="images/gallery/thumb/img-05.jpg" alt="image description">
                                        <div class="tg-hover-pattran">
                                            <a href="images/gallery/thumb/img-05.jpg" data-rel="prettyPhoto[gallery]"><i
                                                    class="flaticon-plus79"></i></a>
                                        </div>
                                    </figure>
                                </li>
                                <li>
                                    <figure>
                                        <img src="images/gallery/thumb/img-06.jpg" alt="image description">
                                        <div class="tg-hover-pattran">
                                            <a href="images/gallery/thumb/img-06.jpg" data-rel="prettyPhoto[gallery]"><i
                                                    class="flaticon-plus79"></i></a>
                                        </div>
                                    </figure>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tg-copyright">
        <div class="container">
            <p>&copy; 2015 | All Rights Reserved</p>
        </div>
    </div>
</footer>
<!--************************************
                                    Footer End
                    *************************************-->