<?php
//session_start();
require_once("php/includeHead.php");
require_once("php/includeScript.php");
require_once("php/db.php");
require_once("php/navHeader.php");
require_once("php/footer.php");
require_once("php/login.php");
$packagesFooter = ReadGeneral('packages', '*', '', 'LIMIT 3');
$galleryFooter = ReadGeneral('footergallery', '*', '', 'LIMIT 6');

?>
<!DOCTYPE html>

<head>
	<?php headInclude(' | Frequently Asked Questions') ?>
</head>

<body>

	<div id="wrapper" class="tg-haslayout">
		<?php navBar(); ?>
		<div class="tg-banner tg-haslayout parallax-window" data-parallax="scroll" data-bleed="100" data-speed="0.2" data-image-src="images/parallax-bg/parallax-img-02.jpg">
			<div class="tg-banner-content">
				<div class="container">
					<ol class="tg-breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li class="active">faq</li>
					</ol>
				</div>
			</div>
		</div>
		<!--************************************
				Inner Pages Banner End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="main" class="tg-haslayout tg-bgwhite">
			<!--************************************
				Blog Start
			*************************************-->
			<div class="tg-main-section tg-haslayout">
				<div class="container">
					<div class="row">
						<div id="tg-twocolumns" class="tg-twocolumns tg-haslayout">
							<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
								<div id="tg-content" class="tg-content tg-faq tg-haslayout">
									<div class="tg-ask-queation tg-haslayout">
										<div class="tg-heading-border">
											<h2>Frequently Asked Question</h2>
											<h3>additional text here</h3>
										</div>
										<div class="tg-panel-group tg-haslayout" id="accordion" role="tablist" aria-multiselectable="true">
											<div class="tg-pannel panel">
												<div class="panel-heading" role="tab" id="headingOne">
													<span>01</span>
													<h4 class="tg-panel-title">
														<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore?</a>
													</h4>
												</div>
												<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
													<div class="tg-panel-body panel-body">
														<ul>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="tg-pannel panel">
												<div class="panel-heading" role="tab" id="headingTwo">
													<span>02</span>
													<h4 class="tg-panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip exea? </a>
													</h4>
												</div>
												<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
													<div class="tg-panel-body panel-body">
														<ul>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="tg-pannel panel">
												<div class="panel-heading" role="tab" id="headingThree">
													<span>03</span>
													<h4 class="tg-panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum?</a>
													</h4>
												</div>
												<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
													<div class="tg-panel-body panel-body">
														<ul>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="tg-pannel panel">
												<div class="panel-heading" role="tab" id="headingFour">
													<span>04</span>
													<h4 class="tg-panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum?</a>
													</h4>
												</div>
												<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
													<div class="tg-panel-body panel-body">
														<ul>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="tg-pannel panel">
												<div class="panel-heading" role="tab" id="headingFive">
													<span>05</span>
													<h4 class="tg-panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum?</a>
													</h4>
												</div>
												<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
													<div class="tg-panel-body panel-body">
														<ul>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="tg-pannel panel">
												<div class="panel-heading" role="tab" id="headingSix">
													<span>06</span>
													<h4 class="tg-panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">Commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum?</a>
													</h4>
												</div>
												<div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
													<div class="tg-panel-body panel-body">
														<ul>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="tg-pannel panel">
												<div class="panel-heading" role="tab" id="headingSeven">
													<span>07</span>
													<h4 class="tg-panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">Commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum?</a>
													</h4>
												</div>
												<div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
													<div class="tg-panel-body panel-body">
														<ul>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="tg-pannel panel">
												<div class="panel-heading" role="tab" id="headingEight">
													<span>08</span>
													<h4 class="tg-panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">Commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum?</a>
													</h4>
												</div>
												<div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
													<div class="tg-panel-body panel-body">
														<ul>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="tg-pannel panel">
												<div class="panel-heading" role="tab" id="headingNine">
													<span>09</span>
													<h4 class="tg-panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">Commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum?</a>
													</h4>
												</div>
												<div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
													<div class="tg-panel-body panel-body">
														<ul>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="tg-pannel panel">
												<div class="panel-heading" role="tab" id="headingTen">
													<span>10</span>
													<h4 class="tg-panel-title">
														<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">Commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum?</a>
													</h4>
												</div>
												<div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
													<div class="tg-panel-body panel-body">
														<ul>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
															<li>
																<div class="tg-heading-border">
																	<h3>
																		<a href="#">Exercitation ullamco laboris nisi ut aliquip?</a>
																	</h3>
																</div>
																<div class="tg-description">
																	<p>Magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</p>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
								<aside id="tg-sidebar" class="tg-sidebar tg-haslayout">
									<div class="tg-widget tg-searchwidget">
										<div class="tg-heading-border">
											<h3>Search widget</h3>
										</div>
										<form class="form-search">
											<fieldset>
												<input type="search" class="form-control" placeholder="Search Here">
												<button class="tg-btn-search fa fa-search"></button>
											</fieldset>
										</form>
									</div>
									<div class="tg-widget tg-widgetcategory">
										<div class="tg-heading-border">
											<h3>category widget</h3>
										</div>
										<ul>
											<li>
												<a href="#">About Us</a>
											</li>
											<li>
												<a href="#">Delivery Information</a>
											</li>
											<li>
												<a href="#">Terms and Conditions</a>
											</li>
											<li>
												<a href="#">Privacy Policy</a>
											</li>
											<li>
												<a href="#">Photography</a>
											</li>
											<li>
												<a href="#">Contact Us</a>
											</li>
											<li>
												<a href="#">Return Policy</a>
											</li>
										</ul>
									</div>
									<div class="tg-widget tg-blogwidget">
										<div class="tg-heading-border">
											<h3>recent post</h3>
										</div>
										<ul>
											<li class="tg-post">
												<figure class="tg-post-img">
													<img src="images/blog/sidebar/img-01.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
												<div class="tg-post-content">
													<h3><a href="#">start exploring</a></h3>
													<div class="tg-description">
														<p>This is Photoshop's version of Lorem ipsum...</p>
													</div>
												</div>
											</li>
											<li class="tg-post">
												<figure class="tg-post-img">
													<img src="images/blog/sidebar/img-02.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
												<div class="tg-post-content">
													<h3><a href="#">life is about fun</a></h3>
													<div class="tg-description">
														<p>This is Photoshop's version of Lorem ipsum...</p>
													</div>
												</div>
											</li>
											<li class="tg-post">
												<figure class="tg-post-img">
													<img src="images/blog/sidebar/img-03.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
												<div class="tg-post-content">
													<h3><a href="#">live like a king</a></h3>
													<div class="tg-description">
														<p>This is Photoshop's version of Lorem ipsum...</p>
													</div>
												</div>
											</li>
											<li class="tg-post">
												<figure class="tg-post-img">
													<img src="images/blog/sidebar/img-04.jpg" alt="image description">
													<div class="tg-hover-pattran">
														<a href="#"><i class="flaticon-directional27"></i></a>
													</div>
												</figure>
												<div class="tg-post-content">
													<h3><a href="#">say no to lazy life</a></h3>
													<div class="tg-description">
														<p>This is Photoshop's version of Lorem ipsum...</p>
													</div>
												</div>
											</li>
										</ul>
									</div>
									<div class="tg-widget tg-recentposts">
										<div class="tg-heading-border">
											<h3>recent post</h3>
										</div>
										<ul>
											<li>
												<div class="tg-description">
													<p>Looking cautiously round, to ascerta in that they were not overheard, the two hags cowered nearer to the fire, and chuckled heartily. <a href="#">#Quote</a></p>
												</div>
											</li>
											<li>
												<div class="tg-description">
													<p>Looking cautiously round, to ascerta in that they were not overheard, the two hags cowered nearer to the fire, and chuckled heartily. <a href="#">#Quote</a></p>
												</div>
											</li>
										</ul>
									</div>
								</aside>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--************************************
				Blog End
			*************************************-->
		</main>

		<?php footer($packagesFooter, $galleryFooter); ?>
	</div>
	<?php loginModal(); ?>
	<?php scriptInclude(); ?>
</body>

</html>