<?php
session_start();
require_once("php/includeHead.php");
require_once("php/includeScript.php");
require_once("php/db.php");
require_once("php/navHeader.php");
require_once("php/footer.php");
require_once("php/login.php");

$packages = ReadGeneral('packages', '*', '', '');
$packagesFooter = ReadGeneral('packages', '*', '', 'LIMIT 3');
$galleryFooter = ReadGeneral('footergallery', '*', '', 'LIMIT 6');
$destinations = ReadGeneral('destinations', '*', '', '');
$bannersliders = ReadGeneral('bannerslider', '*', '', '');
$chooseus = ReadGeneral('chooseus', '*', '', '');
$cstFeedbacks = ReadGeneral('cstFeedback', '*', '', '');
$blogPosts = ReadGeneral('blog', '*', '', '');


// print_r($packages);

?>

<!DOCTYPE html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
  <?php headInclude('') ?>
  <script>
    $(document).ready(function() {
      $('#loginIncorrect').hide();
    });
  </script>
</head>

<body>
  <!--************************************
                        Wrapper Start
        *************************************-->
  <div id="wrapper" class="tg-haslayout">
    <?php navBar(); ?>

    <!--************************************
                                Main Start
                *************************************-->
    <main id="main" class="tg-haslayout">
      <!--************************************
                            Home Slider Start
            *************************************-->
      <div id="tg-home-slider" class="tg-home-slider">
        <div class="parallax-bg" style="
              background-image: url(images/parallax-bg/parallax-img-01.jpg);
            " data-swiper-parallax="-5%"></div>
        <div class="swiper-wrapper">
          <?php
          //loop twice to repeat 3 records in DB
          //for ($x = 0; $x <= 2; $x++) {
          //loop through all DB rows and create destination card for each
          foreach ($bannersliders as $bannerslider) {
          ?>
            <div class="swiper-slide">
              <div class="tg-banner-heading">
                <h1><span><?php echo htmlspecialchars($bannerslider['title']); ?></span><sub>From: $<?php echo htmlspecialchars($bannerslider['minPrice']); ?></sub></h1>
              </div>
              <div class="tg-btnarea">
                <div class="tg-box">
                  <a class="tg-btn" href="#">read more</a>
                </div>
              </div>
            </div>
          <?php }
          //}
          ?>
        </div>
        <div class="swiper-pagination"></div>
        <div class="swiper-button-prev swiper-button-white"></div>
        <div class="swiper-button-next swiper-button-white"></div>
      </div>
      <!--************************************
                                Home Slider End
                *************************************-->

      <!--************************************
                                    Buy Now Start
                    *************************************-->
      <section class="tg-main-section tg-haslayout">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-xs-12">
              <div class="tg-section-head">
                <div class="tg-section-heading">
                  <h2>top Destinations</h2>
                </div>
                <div class="tg-description">
                  <p>
                    Fly into the new decade with a trip to one of these top destinations.
                  </p>
                </div>
              </div>
            </div>
            <div id="tg-destination-slider" class="tg-destination-slider tg-destination">
              <?php
              //loop twice to repeat 3 records in DB
              for ($x = 0; $x <= 2; $x++) {
                //loop through all DB rows and create destination card for each
                foreach ($destinations as $destination) {
              ?>
                  <figure class="item">
                    <a href="#">
                      <img src="<?php echo htmlspecialchars($destination['URL']); ?>" alt="image description" />
                    </a>
                    <figcaption>
                      <span class="tg-icon <?php echo htmlspecialchars($destination['flatIcon']); ?>"></span>
                      <div class="tg-heading-border">
                        <h3><a href="#"><?php echo htmlspecialchars($destination['title']); ?></a></h3>
                      </div>
                      <div class="tg-description">
                        <p>
                          <?php echo htmlspecialchars($destination['description']); ?>.
                        </p>
                      </div>
                      <div class="tg-stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-o"></i>
                      </div>
                    </figcaption>
                  </figure>
              <?php }
              }
              ?>

            </div>
          </div>
        </div>
      </section>
      <!--************************************
                                    Buy Now End
                    *************************************-->
      <!--************************************
                                    Why Chose us Start
                    *************************************-->
      <section class="tg-haslayout tg-positionrelative">
        <div class="tg-videos tg-positionabsolute tg-bglight pull-right">
          <div class="container">
            <div class="col-md-6 col-sm-7 col-xs-12 pull-right">
              <div class="row">
                <div class="tg-whychoseus">
                  <div class="tg-heading-border">
                    <h2>why people like us?</h2>
                    <h3>Why choose us?</h3>
                  </div>
                  <ul>
                    <li>
                      Consectetur adipisicing elit sed do eiusmod tempor
                      incididunt.
                    </li>
                    <li>
                      Labore et dolore magna aliqua enim ad minim veniam.
                    </li>
                    <li>
                      Quis nostrud exercitation ullamco laboris ut aliquip
                      exea commodo.
                    </li>
                    <li>
                      Consequat duis aute irure dolor in reprehenderit in
                      voluptate.
                    </li>
                    <li>
                      Velit esse cillum dolore eu fugiat nulla pariatur.
                    </li>
                  </ul>
                  <div class="tg-btnsarea">
                    <a class="tg-btn" href="#">buy now</a>
                    <a class="tg-btn" href="#">read more</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-12 pull-left">
              <div class="row">
                <div id="tg-video-slider" class="tg-video-slider tg-videobox">
                  <div class="swiper-wrapper">
                    <?php
                    //loop twice to repeat 3 records in DB
                    //for ($x = 0; $x <= 2; $x++) {
                    //loop through all DB rows and create destination card for each
                    foreach ($chooseus as $reason) {
                    ?>
                      <div class="swiper-slide">
                        <div class="tg-pattran"></div>
                        <img class="tg-chooseus-img" src="<?php echo htmlspecialchars($reason['URL']); ?>" alt="image description" />
                        <a href="<?php echo htmlspecialchars($reason['URLVideo']); ?>" data-rel="prettyPhoto" title=""><img src="images/btn-play.png" alt="YouTube" width="60" /></a>
                      </div>
                    <?php }
                    //}
                    ?>
                  </div>
                  <div class="swiper-pagination"></div>
                  <div class="swiper-button-prev swiper-button-white"></div>
                  <div class="swiper-button-next swiper-button-white"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!--************************************
                                    Why Chose us End
                    *************************************-->
      <!--************************************
                                    Packages Start
                    *************************************-->
      <section class="tg-main-section tg-haslayout">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-xs-12">
              <div class="tg-section-head">
                <div class="tg-section-heading">
                  <h2>fair price packages</h2>
                </div>
                <div class="tg-description">
                  <p>
                    Consectetur adipisicing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Ut enim ad
                    minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div id="tg-packages-slider" class="tg-packages-slider tg-packages">
            <?php
            //loop once to repeat 4 records in DB
            for ($x = 0; $x <= 1; $x++) {
              //loop through all DB rows and create package card for each
              foreach ($packages as $package) {
            ?>
                <div class="item">
                  <figure class="tg-package">
                    <img src="<?php echo htmlspecialchars($package['URL']); ?>" alt="image description" />
                    <figcaption>
                      <div class="tg-heading-border">
                        <h3><?php echo htmlspecialchars($package['discount']);  ?>%</h3>
                      </div>
                      <h4><a href="#"><?php echo htmlspecialchars($package['title']);  ?></a></h4>
                      <div class="tg-description">
                        <p>
                          <?php echo htmlspecialchars($package['infoText']);  ?>
                        </p>
                      </div>
                      <a href="#"><i class="flaticon-directional27"></i></a>
                    </figcaption>
                  </figure>
                </div>
            <?php }
            }
            ?>
          </div>
        </div>
      </section>
      <!--************************************
                                    Packages End
                    *************************************-->
      <!--************************************
                                    Testimonials Start
                    *************************************-->
      <section class="tg-main-section tg-haslayout tg-bglight">
        <div class="container">
          <div class="row">
            <div class="col-md-8 col-sm-12 col-xs-12 pull-right">
              <div class="tg-testimonials-box">
                <div class="tg-heading-border">
                  <h2>customer feedback</h2>
                  <h3>our testimonials</h3>
                </div>
                <div class="tg-testimonials-sliderbox">
                  <div class="col-sm-6">
                    <div id="tg-testimonials-message-slider" class="tg-testimonials-message-slider tg-testimonials-message">
                      <?php
                      //loop twice to repeat 3 records in DB
                      //for ($x = 0; $x <= 2; $x++) {
                      //loop through all DB rows and create destination card for each
                      foreach ($cstFeedbacks as $cstFeedback) {
                      ?>
                        <div class="item">
                          <div class="tg-heading-border">
                            <h4><?php echo htmlspecialchars($cstFeedback['title']); ?></h4>
                          </div>
                          <div class="tg-description">
                            <blockquote>
                              <q><?php echo htmlspecialchars($cstFeedback['cstComment']); ?>.</q>
                            </blockquote>
                          </div>
                        </div>
                      <?php }
                      //}
                      ?>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div id="tg-testimonials-pagger-slider" class="tg-testimonials-pagger-slider tg-testimonials-pagger">
                      <?php
                      //loop twice to repeat 3 records in DB
                      //for ($x = 0; $x <= 2; $x++) {
                      //loop through all DB rows and create destination card for each
                      foreach ($cstFeedbacks as $cstFeedback) {
                      ?>
                        <div class="item">
                          <img src="<?php echo htmlspecialchars($cstFeedback['URL']); ?>" alt="image description" />
                          <div class="tg-hover-pattran">
                            <i class="flaticon-plus79"></i>
                          </div>
                        </div>
                      <?php }
                      //}
                      ?>
                    </div>
                    <a class="tg-btn-viewall" href="#">view all</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12 pull-left">
              <figure class="tg-imgbox">
                <img src="images/img-01.jpg" alt="image description" />
              </figure>
            </div>
          </div>
        </div>
      </section>
      <!--************************************
                                    Testimonials End
                    *************************************-->
      <!--************************************
                                    Blog Post Start
                    *************************************-->
      <section class="tg-main-section tg-haslayout">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-xs-12">
              <div class="tg-section-head">
                <div class="tg-section-heading">
                  <h2>latest blog</h2>
                </div>
                <div class="tg-description">
                  <p>
                    Consectetur adipisicing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Ut enim ad
                    minim veniam, quis nostrud exercitation ullamco laboris
                    nisi ut aliquip ex ea commodo consequat.
                  </p>
                </div>
              </div>
            </div>
            <div id="tg-post-slider" class="tg-post-slider tg-blogpost tg-haslayout">
              <?php
              //loop twice to repeat 3 records in DB
              for ($x = 0; $x <= 1; $x++) {
                //loop through all DB rows and create destination card for each
                foreach ($blogPosts as $blogPost) {
                  $date = DateTime::createFromFormat('Y-m-d', $blogPost['datePosted']);
              ?>
                  <div class="item">
                    <div class="row">
                      <div class="tg-post">
                        <div class="col-lg-4 col-md-5 col-sm-12">
                          <figure class="tg-post-img">
                            <img src="<?php echo htmlspecialchars($blogPost['URL']); ?>" alt="image description" />
                            <div class="tg-hover-pattran">
                              <a href="#"><i class="flaticon-directional27"></i></a>
                            </div>
                          </figure>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-12">
                          <div class="tg-post-content">
                            <ul class="tg-postmata">
                              <li>
                                <time datetime="<?php echo htmlspecialchars($blogPost['datePosted']); ?>"><a href="#"><?php echo htmlspecialchars($date->format('M, d Y'), ENT_QUOTES, "UTF-8"); ?></a></time>
                              </li>
                            </ul>
                            <div class="tg-heading-border">
                              <h3>
                                <a href="#"><?php echo htmlspecialchars($blogPost['title']); ?></a>
                              </h3>
                            </div>
                            <div class="tg-description">
                              <p>
                                <?php echo htmlspecialchars($blogPost['description']); ?>
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              <?php }
              }
              ?>
            </div>
          </div>
        </div>
      </section>
      <!--************************************
                                    Blog Post End
                    *************************************-->
    </main>
    <!--************************************
                                Main End
                *************************************-->

    <?php footer($packagesFooter, $galleryFooter); ?>
  </div>
  <?php loginModal(); ?>
  <?php scriptInclude(); ?>

  <!-- scripts are here -->

</body>

</html>