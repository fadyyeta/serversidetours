<?php
//session_start();
require_once("php/includeHead.php");
require_once("php/includeScript.php");
require_once("php/db.php");
require_once("php/navHeader.php");
require_once("php/footer.php");
require_once("php/login.php");
$packagesFooter = ReadGeneral('packages', '*', '', 'LIMIT 3');
$galleryFooter = ReadGeneral('footergallery', '*', '', 'LIMIT 6');

?>
<!DOCTYPE html>

<head>
	<?php headInclude(' | Contact Us') ?>
</head>

<body>

	<div id="wrapper" class="tg-haslayout">
		<?php navBar(); ?>
		<div class="tg-banner tg-haslayout parallax-window" data-parallax="scroll" data-bleed="100" data-speed="0.2" data-image-src="images/parallax-bg/parallax-img-02.jpg">
			<div class="tg-banner-content">
				<div class="container">
					<ol class="tg-breadcrumb">
						<li><a href="index.php">Home</a></li>
						<li class="active">contact us</li>
					</ol>
				</div>
			</div>
		</div>
		<!--************************************
				Inner Pages Banner End
		*************************************-->
		<!--************************************
				Main Start
		*************************************-->
		<main id="main" class="tg-haslayout">
			<div class="tg-relativepostion tg-haslayout">
				<div class="tg-relativepostion tg-haslayout">
					<div class="tg-contact-form tg-haslayout">
						<div class="tg-displaytable">
							<div class="tg-displaytablecell">
								<div class="container">
									<div class="row">
										<div class="col-md-6 col-sm-12 col-xs-12 pull-left">
											<div class="tg-heading-border">
												<h2>get in touch</h2>
												<h3>We love to hear from you</h3>
											</div>
											<form class="tg-form-contact tg-haslayout">
												<fieldset>
													<div class="row">
														<div class="col-sm-6">
															<div class="row">
																<div class="col-sm-12">
																	<div class="form-group">
																		<input type="text" class="form-control" placeholder="Name">
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="form-group">
																		<input type="text" class="form-control" placeholder="Phone">
																	</div>
																</div>
																<div class="col-sm-12">
																	<div class="form-group">
																		<input type="text" class="form-control" placeholder="Company">
																	</div>
																</div>
															</div>
														</div>
														<div class="col-xs-12">
															<div class="form-group">
																<textarea class="form-control" placeholder="Message"></textarea>
															</div>
														</div>
														<div class="col-xs-12">
															<button class="tg-btn">Send</button>
														</div>
													</div>
												</fieldset>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12 pull-right">
						<div class="row">
							<div class="tg-mapbox">
								<div id="tg-location-map" class="tg-location-map tg-haslayout"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<!--************************************
				Partners Start
		*************************************-->
			<section class="tg-main-section tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-sm-offset-2 col-xs-12">
							<div class="tg-section-head">
								<div class="tg-section-heading">
									<h2>Our Partners</h2>
								</div>
								<div class="tg-description">
									<p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								</div>
							</div>
						</div>
						<div id="tg-partners-slider" class="tg-partners-slider tg-partners">
							<figure class="item tg-partner">
								<a href="#">
									<img src="images/partners/img-01.jpg" alt="image description">
								</a>
							</figure>
							<figure class="item tg-partner">
								<a href="#">
									<img src="images/partners/img-02.jpg" alt="image description">
								</a>
							</figure>
							<figure class="item tg-partner">
								<a href="#">
									<img src="images/partners/img-03.jpg" alt="image description">
								</a>
							</figure>
							<figure class="item tg-partner">
								<a href="#">
									<img src="images/partners/img-01.jpg" alt="image description">
								</a>
							</figure>
							<figure class="item tg-partner">
								<a href="#">
									<img src="images/partners/img-02.jpg" alt="image description">
								</a>
							</figure>
							<figure class="item tg-partner">
								<a href="#">
									<img src="images/partners/img-03.jpg" alt="image description">
								</a>
							</figure>
							<figure class="item tg-partner">
								<a href="#">
									<img src="images/partners/img-01.jpg" alt="image description">
								</a>
							</figure>
							<figure class="item tg-partner">
								<a href="#">
									<img src="images/partners/img-02.jpg" alt="image description">
								</a>
							</figure>
							<figure class="item tg-partner">
								<a href="#">
									<img src="images/partners/img-03.jpg" alt="image description">
								</a>
							</figure>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
				Partners End
		*************************************-->
		</main>
		<?php footer($packagesFooter, $galleryFooter); ?>
	</div>
	<?php loginModal(); ?>
	<?php scriptInclude(); ?>
</body>

</html>